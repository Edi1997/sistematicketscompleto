<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Licencia extends Model
{
    
    protected $fillable = [
        'nombre_programa', 'digitos', 'version','subversion',
     ];
     public function Equipo(){
        return $this->belongsToMany('App\Equipo');
     }
     public function users(){
      return $this->belongsToMany('App\User');
      }
      public function licenciaequipo(){
         return $this->belongsToMany('App\Licencia_equipo');
      }
     
}

