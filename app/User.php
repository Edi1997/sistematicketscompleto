<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password','apellidoP','apellidoM','edificio','extension','usuario','departamento',
    ];
    public function Equipo(){
        return $this->hasMany('App\Equipo');
     }
     public function licencias(){
        return $this->belongsToMany('App\Licencia');
   }
   public function licenciasequipo(){
    return $this->belongsToMany('App\Licencia_equipo');
}
   
//    public function has_licencia($id){
//     foreach($this->licencias as $licencia){
//     if($licencia->id ==$id)return true;
//    }
//   return false;
//  }
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];
}
