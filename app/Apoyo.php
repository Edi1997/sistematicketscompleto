<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Apoyo extends Model
{
    //
    public $table = 'apoyos';
    protected $fillable = [
        'numerocontrol','carrera ','nombre','apellidoPaterno','apellidoMaterno','opcion','descripcion','email','foto','captcha','order_nr','estatus',
     ];
}
