<?php

namespace App\Http\Controllers;

use App\Apoyo;
use Illuminate\Http\Request;
use App\Http\Requests\CreateApoyoValidation;
use Illuminate\Support\MessageBag;
use Illuminate\Support\Facades\DB;

class ApoyoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return view('formularioapoyo');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateApoyoValidation $request)
    {
        //
        
        $request->validated();
        $datos=request()->except('_token');
        if($request->hasFile('foto')){
          
             $datos['foto']=$request->file('foto')->store('/','public');
             
           
          }
       ///Generar el numero aleatorio de atencion
          $characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
          $pin = mt_Rand(1000000, 9999999)
              . mt_Rand(1000000, 9999999)
              . $characters[Rand(0, strlen($characters) - 1)];
          $generateOrder_nr = str_shuffle($pin);
        $datos['order_nr']=$generateOrder_nr;
        $datos['created_at']=now();
       
        Apoyo::insert($datos);
        
        return view('esperaAprob',compact('generateOrder_nr')); 
        //  return redirect()->route('apoyo.index')->with('Success','Elementos guardados'); 
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Apoyo  $apoyo
     * @return \Illuminate\Http\Response
     */
    public function refreshCaptcha(){
        
        return captcha_img('flat');
    }
    public function show()
    {
  
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Apoyo  $apoyo
     * @return \Illuminate\Http\Response
     */
    public function edit(Apoyo $apoyo)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Apoyo  $apoyo
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,  $id)
    {
        //
        $datos=Apoyo::findOrFail($id);
        $datos->estatus=$request->estatus;
        $datos->save();
        return redirect('correos');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Apoyo  $apoyo
     * @return \Illuminate\Http\Response
     */
    public function destroy(Apoyo $apoyo)
    {
        //
    }
}
