<?php

namespace App\Http\Controllers;

use App\Mantenimiento;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Auth; 
use App\User;
use Barryvdh\DomPDF\Facade as PDF;
class MantenimientoController extends Controller
{
    public function getViewRole($vista){
        $user=Auth::user()->id;
        $rol=DB::table('usuariorol')
        ->select('usuariorol.idRol')
        ->where('usuariorol.idUsuario', ($user))
        ->get();
        switch ($rol[0]->idRol) {
            case 1:
            $return=("admin.".$vista."");
            return ($return);
                // Administrador
            break;
            case 2:
            $return=("helpdesk.".$vista."");
            return ($return);
                // HelpDesk
            break;
            case 3:
            
            $return=("agente.".$vista."");
            return ($return);
                // Agente 1er nivel
            break;
            case 4:
            $return=("agente.".$vista."");
            return ($return);
                // Agente 2do nivel
            break;
            case 5:
            $return=("usuario.".$vista."");
            return ($return);
            break;
            default:
            $return=("usuario.".$vista."");
            return ($return);
            break;
        }
        
    }
    public function __construct(){
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $mantenimientos=Mantenimiento::all();
        // $licenciasusuario=Licencia_equipo::all();
        return view(self::getViewRole('mantenimientos'),compact('mantenimientos')); 
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }
    public function create_pdf($id){
        $mantenimientos=Mantenimiento::findOrFail($id);
         $pdf=PDF::loadView(self::getViewRole('solicitudpdf'),compact('mantenimientos'));  
        return $pdf->stream();
     
      }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $mantenimiento=Mantenimiento::create([
            'fecha' => $request['fecha'],
            'area' => $request['area'],
            'responsable' => $request['responsable'],
            'tiposervicio' => $request['tiposervicio'],
            'descripcion' => $request['descripcion'],
            'serviciorealizado' => $request['serviciorealizado'],
            'material' => $request['material'],
            'solicitante' => $request['solicitante'],
            'realizador' => $request['realizador'],
            'recibidor' => $request['recibidor'],
           
    ]);
   
    return redirect()->route('mantenimiento.index')->with('Success','Elementos guardados'); 
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Mantenimiento  $mantenimiento
     * @return \Illuminate\Http\Response
     */
    public function show(Mantenimiento $mantenimiento)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Mantenimiento  $mantenimiento
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $mantenimiento=Mantenimiento::findOrFail($id);
        return view(self::getViewRole('editmantenimiento'),compact('mantenimiento'));
  
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Mantenimiento  $mantenimiento
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$id)
    {
        //
       
        $mantenimiento=Mantenimiento::findOrFail($id);
        $mantenimiento->fecha=$request->fecha;
        $mantenimiento->area=$request->area;
        $mantenimiento->responsable=$request->responsable;
        $mantenimiento->tiposervicio=$request->tiposervicio;
        $mantenimiento->descripcion=$request->descripcion;
        $mantenimiento->serviciorealizado=$request->serviciorealizado;
        $mantenimiento->material=$request->material;
        $mantenimiento->solicitante=$request->solicitante;
        $mantenimiento->realizador=$request->realizador;
        $mantenimiento->recibidor=$request->recibidor;
        $mantenimiento->save();
        return redirect('mantenimiento');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Mantenimiento  $mantenimiento
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        Mantenimiento::destroy($id);
        return redirect('mantenimiento');
    }
}
