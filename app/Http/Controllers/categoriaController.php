<?php

namespace App\Http\Controllers;
use Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Categoria;

class categoriaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getViewRole($vista){
        $user=Auth::user()->id;
        
        $rol=DB::table('usuariorol')
        ->select('usuariorol.idRol')
        ->where('usuariorol.idUsuario', ($user))
        ->get();
        switch ($rol[0]->idRol) {
            case 1:
            $return=("admin.".$vista."");
            return ($return);
                // Administrador
            break;
            case 2:
            $return=("helpdesk.".$vista."");
            return ($return);
                // HelpDesk
            break;
            case 3:
            
            $return=("agente.".$vista."");
            return ($return);
                // Agente 1er nivel
            break;
            case 4:
            $return=("agente.".$vista."");
            return ($return);
                // Agente 2do nivel
            break;
            case 5:
            $return=("usuario.".$vista."");
            return ($return);
            break;
            default:
            $return=("usuario.".$vista."");
            return ($return);
            break;
        }
        
    }

    public function index()
    {
        $categorias = DB::table('categoriaReporte')
        ->orderByRaw('categoriaReporte','ASC')
        ->get();
        return view(self::getViewRole('categorias'), compact('categorias'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        categoria::create([
            'categoriaReporte'=>request('categoria'),
            'descripcion'=>request('descripcion')
        ]);   
        return redirect('categorias'); 
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        
        $categoria=DB::table('categoriaReporte')->where('id', '=', request('id'))->get();
        return view(self::getViewRole('editarCategoria'), compact('categoria'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        
        DB::table('categoriaReporte')
              ->where('id', request('id'))
              ->update(['categoriaReporte' => request('categoria'),
                       'descripcion' => request('descripcion')]);

        return redirect('categorias');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function delete(Request $request){
        
        DB::table('categoriaReporte')->where('id', '=', request('id'))->delete();
        return redirect('categorias');
    }
}
