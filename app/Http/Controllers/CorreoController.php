<?php

namespace App\Http\Controllers;

use App\Correo;
use App\Apoyo;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Auth; 
use Response;
use Illuminate\Support\Facades\Storage;

class CorreoController extends Controller
{ 
    public function getViewRole($vista){
        $user=Auth::user()->id;
        $rol=DB::table('usuariorol')
        ->select('usuariorol.idRol')
        ->where('usuariorol.idUsuario', ($user))
        ->get();
        switch ($rol[0]->idRol) {
            case 1:
            $return=("admin.".$vista."");
            return ($return);
                // Administrador
            break;
            case 2:
            $return=("helpdesk.".$vista."");
            return ($return);
                // HelpDesk
            break;
            case 3:
            
            $return=("agente.".$vista."");
            return ($return);
                // Agente 1er nivel
            break;
            case 4:
            $return=("agente.".$vista."");
            return ($return);
                // Agente 2do nivel
            break;
            case 5:
            $return=("usuario.".$vista."");
            return ($return);
            break;
            default:
            $return=("usuario.".$vista."");
            return ($return);
            break;
        }
        
    }
    public function __construct(){
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $datosNoValidados=Apoyo::all();
        
        return view(self::getViewRole('datosNovalidados'),compact('datosNoValidados')); 
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
       
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $datos=request()->except('_token');
        // foreach($datos as $row) {
        //     echo "<ul><li>".implode("</li><li>", $row). "</li></ul>";

        //            }

     $filename = "apoyosCorreo.csv";
        $handle = fopen($filename, 'w+');
      fputcsv($handle, array('','numero de control', 'carrera','apellido paterno','apellido materno','nombre','peticion','descripcion del problema'));
         foreach($datos as $row) {
              fputcsv($handle,$row,"\n");
         }
          fclose($handle); 
     
         $headers = array(
             'Content-Type' => 'text/csv',
       );
     
     return Response::download($filename, 'apoyosCorreo.csv', $headers);
    
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Correo  $correo
     * @return \Illuminate\Http\Response
     */
    public function show( $id)
    {
        //
        $datos=Apoyo::findOrFail($id);
        return view(self::getViewRole('asignaestatus'),compact('datos'));
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Correo  $correo
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $datos=Apoyo::findOrFail($id);
        return view(self::getViewRole('editCorreo'),compact('datos'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Correo  $correo
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $datos=request()->except(['_token','_method']);
        if($request->hasFile('foto')){
            $correos=Apoyo::findOrFail($id);
            Storage::delete('public/'.$correos->foto);

            $datos['foto']=$request->file('foto')->store('/','public');
         }
        Apoyo::where('id','=',$id)->update($datos);
       
        return redirect('correos');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Correo  $correo
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $correos=Apoyo::findOrFail($id);
        if(Storage::delete('public/'.$correos->foto)){
            Apoyo::destroy($id);
        }
        return redirect('correos');
    }
}
