<?php

namespace App\Http\Controllers;

use App\Licencia;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Auth; 


class LicenciaController extends Controller
{
    public function __construct(){
        $this->middleware('auth');
    }
    public function getViewRole($vista){
        $user=Auth::user()->id;
        $rol=DB::table('usuariorol')
        ->select('usuariorol.idRol')
        ->where('usuariorol.idUsuario', ($user))
        ->get();
        switch ($rol[0]->idRol) {
            case 1:
            $return=("admin.".$vista."");
            return ($return);
                // Administrador
            break;
            case 2:
            $return=("helpdesk.".$vista."");
            return ($return);
                // HelpDesk
            break;
            case 3:
            
            $return=("agente.".$vista."");
            return ($return);
                // Agente 1er nivel
            break;
            case 4:
            $return=("agente.".$vista."");
            return ($return);
                // Agente 2do nivel
            break;
            case 5:
            $return=("usuario.".$vista."");
            return ($return);
            break;
            default:
            $return=("usuario.".$vista."");
            return ($return);
            break;
        }
        
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        
       $licencias=Licencia::paginate(10);
       return view(self::getViewRole('licencias'),compact('licencias'));
       // return view('licencia.licencias',compact('licencias'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $licencias=Licencia::create([
            'nombre_programa' => $request['nombre_programa'],
            'digitos' => $request['digitos'],
            'version' => $request['version'],
            'subversion' => $request['subversion'],
           
    ]);
   
    return redirect()->route('altalicencias.index')->with('Success','Elementos guardados'); 
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Licencia  $licencia
     * @return \Illuminate\Http\Response
     */
    public function show(Licencia $licencia)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Licencia  $licencia
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $licencias=Licencia::findOrFail($id);
        return view(self::getViewRole('editlicencia'),compact('licencias'));
        //return view('licencia/edit',compact('licencias'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Licencia  $licencia
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $licencias=Licencia::findOrFail($id);
        $licencias->nombre_programa=$request->nombre_programa;
        $licencias->digitos=$request->digitos;
        $licencias->version=$request->version;
        $licencias->subversion=$request->subversion;
        $licencias->save();
        return redirect('altalicencias');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Licencia  $licencia
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        Licencia::destroy($id);
        return redirect('altalicencias');
    }
}
