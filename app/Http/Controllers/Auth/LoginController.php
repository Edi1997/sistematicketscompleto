<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    
    function redirectTo(){
        $redirectTo = '/tickets';    
        $user=Auth::user()->id;
        $rol=DB::table('usuariorol')
        ->select('usuariorol.idRol')
        ->where('usuariorol.idUsuario', ($user))
        ->get();
        switch ($rol[0]->idRol) {
            case 1:
                return 'tickets';
                // Administrador
                break;
            case 2:
                return 'tickets';
                // HelpDesk
                break;
            case 3:
                return 'tickets';
                // Agente 1er nivel
                break;
            case 4:
                return 'tickets';
                // Agente 2do nivel
                break;
            case 5:
                return ('esperaAprob');
                break;
            default:
                return 'seguimiento';
                # code...
                break;
        }

        
    }

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }
}
