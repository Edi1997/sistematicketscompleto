<?php

namespace App\Http\Controllers;

use App\Licencia_equipo;
use App\Equipo;
use App\Licencia;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use App\User;
use Auth; 

class LicenciaEquipoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getViewRole($vista){
        $user=Auth::user()->id;
        $rol=DB::table('usuariorol')
        ->select('usuariorol.idRol')
        ->where('usuariorol.idUsuario', ($user))
        ->get();
        switch ($rol[0]->idRol) {
            case 1:
            $return=("admin.".$vista."");
            return ($return);
                // Administrador
            break;
            case 2:
            $return=("helpdesk.".$vista."");
            return ($return);
                // HelpDesk
            break;
            case 3:
            
            $return=("agente.".$vista."");
            return ($return);
                // Agente 1er nivel
            break;
            case 4:
            $return=("agente.".$vista."");
            return ($return);
                // Agente 2do nivel
            break;
            case 5:
            $return=("usuario.".$vista."");
            return ($return);
            break;
            default:
            $return=("usuario.".$vista."");
            return ($return);
            break;
        }
        
    }
    public function __construct(){
        $this->middleware('auth');
    }
    public function index()
    {
        //
         $licenciasUsuario=Equipo::all();
         return view(self::getViewRole('equiposlicencias'),compact('licenciasUsuario'));
        // $users=user::all();
        // return view(self::getViewRole('userequipos'),compact('users'));
    }
    public function numero($id)
    {
        //
        // $licenciasUsuario=Equipo::all();

        $equipo=Equipo::findOrFail($id);
        $n= DB::table('equipo_licencia')
         ->select(DB::raw('count(licencia_id) as n_count '))
         ->from('equipo_licencia')
         ->where('equipo_id',($id))
         ->get();
         $e= $n[0]->n_count; 
        
        return view(self::getViewRole('detalles'),compact('e','equipo'));
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
          //
         // $equipo=Equipo::all();
          $licenciasusuario=Licencia_equipo::all();
          return view(self::getViewRole('licenciasconsulta'),compact('licenciasusuario')); 
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request,$id)
    {
        //
         //
         $licenciasequipo=Equipo::findOrFail($id);
         //dd($licenciasequipo);
        $licenciasIds=$request->licencias;
       $licenciasequipo->licencias()->sync($licenciasIds);
         //$user->licencias()->sync($licenciasIds);
         // $n= DB::table('licencia_user')
         //              ->select(DB::raw('count(licencia_id) as n_count '))
         //              ->from('licencia_user')
         //              ->where('user_id',($id))
         //              ->get();
         //              $e= $n[0]->n_count; 
         return redirect('licencia_equipo');
         //return redirect()->route('index')->with('Success','Elementos guardados'); 
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $licenciasequipo=Equipo::findOrFail($id);
        $licencias=Licencia::all();
     return view(self::getViewRole('equipolice'),compact('licenciasequipo','licencias'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        
        //
    }
}
