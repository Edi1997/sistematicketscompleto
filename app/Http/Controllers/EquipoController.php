<?php

namespace App\Http\Controllers;

use App\Equipo;
use App\Licencia;
use App\Licencia_equipo;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use App\User;
use Auth; 
use Illuminate\Http\Request;

class EquipoController extends Controller
{
    public function getViewRole($vista){
        $user=Auth::user()->id;
        $rol=DB::table('usuariorol')
        ->select('usuariorol.idRol')
        ->where('usuariorol.idUsuario', ($user))
        ->get();
        switch ($rol[0]->idRol) {
            case 1:
            $return=("admin.".$vista."");
            return ($return);
                // Administrador
            break;
            case 2:
            $return=("helpdesk.".$vista."");
            return ($return);
                // HelpDesk
            break;
            case 3:
            
            $return=("agente.".$vista."");
            return ($return);
                // Agente 1er nivel
            break;
            case 4:
            $return=("agente.".$vista."");
            return ($return);
                // Agente 2do nivel
            break;
            case 5:
            $return=("usuario.".$vista."");
            return ($return);
            break;
            default:
            $return=("usuario.".$vista."");
            return ($return);
            break;
        }
        
    }
    public function __construct(){
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $equipos=Equipo::all();
        // $licenciasusuario=Licencia_equipo::all();
        return view(self::getViewRole('licenciasconsulta'),compact('equipos')); 
    }
    public function numero($id)
    {
       
        $equipo=Equipo::findOrFail($id);
         /* $n= DB::table('equipo_licencia')
         ->select(DB::raw('* '))
          ->from('equipo_licencia')
         ->where('equipo_id',($id))
          ->get();
         dd($n); */
         //$e= $n[0]; 
         $n= DB::table('equipo_licencia')
         ->select('licencias.nombre_programa','licencias.digitos','licencias.version','licencias.subversion','equipos.nserie_equipo','users.name','users.departamento')
          ->join('licencias','equipo_licencia.licencia_id','=','licencias.id')
          ->join('equipos','equipo_licencia.equipo_id','=','equipos.id')
          ->join('users','equipos.user_id','=','users.id')
         ->where('equipo_id',($id))
          ->get();
         //return $n;
          
        return view(self::getViewRole('totalicencias'),compact('n','equipo'));
    }
    

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $users=user::all();
        return view(self::getViewRole('licenciasasignacion'),compact('users'));
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request,$id)
    {
        //
        $user=User::findOrFail($id);
        $licenciasusuario=$request->nserie_equipo;
                 DB::table('equipos')
                     ->insert(['user_id' =>$id, 'nserie_equipo' =>$licenciasusuario]
                      );
        return redirect()->route('equipo.create')->with('Success','Elementos guardados'); 
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Equipo  $equipo
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $user=User::findOrFail($id);
        return view(self::getViewRole('asignar_licencia'),compact('user'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Equipo  $equipo
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $licenciasusuario=Equipo::findOrFail($id);
       return view(self::getViewRole('editarasignacion'),compact('licenciasusuario'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Equipo  $equipo
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $licenciasusuario=Equipo::findOrFail($id);
        $licenciasusuario->nserie_equipo=$request->nserie_equipo;
        $licenciasusuario->save();
        return redirect('equipo');
    }
    

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Equipo  $equipo
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        Equipo::destroy($id);
        return redirect('equipo');
    }
}
