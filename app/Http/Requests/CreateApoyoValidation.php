<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateApoyoValidation extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
            'numerocontrol'=>'required',
            'carrera'=>'required',
            'nombre'=>'required',
            'apellidoPaterno'=>'required',
            'apellidoMaterno'=>'required',
            'opcion'=>'required',
            'descripcion'=>'required',
            'email'=>'required',
            'foto' => 'required',
            'captcha' => 'required|captcha'
            
        ];
        
    }
    public function messages(){
        return[
                'numerocontrol.required'=>'El numero de control es requerido',
                'carrera.required'=>'El nombre de la carrera es requerida',
                'nombre.required'=>'El nombre es requerido',
                'apellidoPaterno.required'=>'El Apellido Paterno es requerido',
                'apellidoMaterno.required'=>'El Apellido Materno es requerido',
                'opcion.required'=>'La tipo de apoyo es requerida',
                'descripcion.required'=>'La descripcion del problema es requerido',
                'email.required'=>'El email de contacto es requerido',
                'foto.required' =>'La foto de credencial es requerida',
                'captcha.captcha'=>'Invalid captcha code.',
        ];
    }
}
