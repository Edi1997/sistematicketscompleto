<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Licencia_equipo extends Model
{
    //
    public $table = 'equipo_licencia';
    protected $fillable = [
         'equipo_id','licencia_id',
     ];
     public function licencias(){
        return $this->belongsToMany('App\Licencia');
    }
    public function users(){
        return $this->belongsToMany('App\User');
        }
        public function Equipo(){
            return $this->belongsTo('App\Equipo');
         }
         

}
