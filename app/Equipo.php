<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Equipo extends Model
{
    //
    public $table = 'equipos';
    protected $fillable = [
        'nlicencias_asignadas', 'nserie_equipo', 'user_id',
     ];
     public function user(){
        return $this->belongsTo('App\User');
    }
    public function licencias(){
        return $this->belongsToMany('App\Licencia');
   }
    public function licenciaequipo(){
         return $this->hasMany('App\Licencia_equipo');
      }
      public function has_licencia($id){
        foreach($this->licencias as $licencia){
        if($licencia->id ==$id)return true;
       }
      return false;
     }
}
