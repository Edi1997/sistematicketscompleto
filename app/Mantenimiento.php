<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Mantenimiento extends Model
{
    //
    public $table = 'mantenimientos';
    protected $fillable = [
        'fecha', 'area', 'responsable','tiposervicio','descripcion','serviciorealizado','material','solicitante','realizador','recibidor',
    ];
   
}
