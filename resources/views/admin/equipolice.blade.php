@extends('licenciasasignacion')
@include('layouts.navbar')
@include('layouts.sidebarAdmin') 
@section('solicitudes-tabla')
<div class="card mb-3">
 <h5>Agregar Licencia al equipo del usuario: {{$licenciasequipo->user->name}}<br>
Departamento {{$licenciasequipo->user->departamento}}</h5>
<h5>Numero de serie del equipo (Activo): {{$licenciasequipo->nserie_equipo}}</h5>
<h5>Licencias Disponibles: </h5> 
<br> 
<form style="margin-left:180px" class="col s12" method="post" action="{{ route('licencia_equipo.store',$licenciasequipo->id )}}">
 @csrf
  @foreach($licencias as $licencia )

       <div class="form-row">
      <input  type="checkbox" name="licencias[]" value="{{ $licencia->id }}" class="filled-in"  id="{{ $licencia->id }}"@if($licenciasequipo->has_licencia($licencia->id)) checked @endif/>
       <label   for= "{{ $licencia->id }}">
      <span>{{ $licencia->nombre_programa }} {{ $licencia->version }} {{ $licencia->subversion }}</span> 
      </div>  
       {{-- <span>{{ $licencia->version }}</span> --}}
       {{-- <span>{{ $licencia->subversion }}</span> --}}
      </label>
 @endforeach  
 


 {{-- <div class="row form-group">
                        <div class="col-3">
                            <label for="tipo"> Licencia: </label>
                        </div>
                        <div class="col-5">
                            <select class="custom-select" multiple name="licencias[]">
                             <option value="" disabled selected>Escoge tu opciones presionando ctrl</option>
                            @foreach($licencias as $licencia )
                                 <option id="{{ $licencia->id }}" value="{{ $licencia->id }}" @if($user->has_licencia($licencia->id)) disabled selected @endif> {{ $licencia->nombre_programa }}</option> 
                                
                            @endforeach
                            </select>
                           
                        </div>
                    </div>  --}}


            
         <br> <div class="footer">
                {{-- <button type="submit" class="btn btn-primary" > Aceptar </button> --}}
                <button class="btn btn-primary" type="submit" onclick="return confirm('¿Guardar? Verifique antes que la información del equipo este correcta')">Aceptar </button>
            </div>
            
        </div>
         {{-- <a class="btn btn-warning" href="{{url('/licencia_usuario/create')}}" > Regresar</a> --}}
        </div>
         
 </form>
 
 </div>
@endsection