@extends('solicitud-ticket')
@include('layouts.navbar')
@include('layouts.sidebarAdmin')
@section('filtro')
@endsection

@section('solicitudes')
    <div class="row">
    @foreach($ticket as $contacto)
        <div class="col-xl-3 col-sm-6 mb-3">
            <div class="card text-white bg-warning o-hidden h-100">
                <div class="card-body">
                    <div class="card-body-icon">
                        <i class="fas fa-fw fa-comments"></i>
                    </div>
                  
   
                    <div class="mr-5 text-center"> # Solicitud </div>
                    <div class="mr-5"> Solicitante: {{$contacto->name}} - </div>
                    <div class="mr-5"> Equipo: {{$contacto->equipoReportado}} </div>
                    <div class="mr-5"> Tipo: {{$contacto->tipoReporte}}</div>
                    <div class="mr-5"> Fecha: {{$contacto->created_at}}</div>
                    <div class="mr-5"> Categoria: {{$contacto->categoriaReporte}}</div>
                    <div class="mr-5"> Prioridad: {{$contacto->nivel}}</div>
                    
                </div>
                

                <a class="card-footer text-white clearfix small z-1" data-toggle="modal" data-target="#modalsolicitud"  data-name="{{$contacto->name}}"
                data-ticket="{{$contacto->id}}"
                data-equipo="{{$contacto->equipoReportado}}"
                data-tipo="{{$contacto->tipoReporte}}"
                data-categoria="{{$contacto->categoriaReporte}}"
                data-subcategoria="{{$contacto->subCategoria}}"
                data-descrip="{{$contacto->justificacions}}"
                data-fecha="{{$contacto->created_at}}"
                data-prioridad="{{$contacto->idNivelUrgencia}}"
               
                >

                    <span class="float-left" data-toggle="modal"   data-target="#modalsolicitud" > Ver </span>
                    <span class="float-right">
                        <i class="fas fa-angle-right"></i>
                    </span>
                </a>
            
            </div>
           
        </div>
        @endforeach
    </div>
@endsection

@section('modalsolicitud')
<form action="{{url('/solicitudes-tickets/aceptarTicket')}}" method="post" >
            {{csrf_field()}}
    <div class="modal fade" id="modalsolicitud" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header text-center">
              <h5 class="modal-title text-center" id="exampleModalLabel"> SOLICITUD # </h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            
            <div class="modal-body">
            <div class="row">
                    <div class="col-3">
                    </div>
                    <div class="col-9">
                    <input readonly class="form-control-plaintext" type="text"  name="ticket" id="ticket" value="ticket"  style="visibility:hidden">
                    </div>
                </div>

                <div class="row">
                    <div class="col-3">
                    <b> Solicitante: </b>
                    </div>
                    <div class="col-9">
                    <input readonly class="form-control-plaintext" type="text"  name="name" id="name" value="name" style="height:70%;">
                    </div>
                </div>
                <div class="row">
                    <div class="col-3">
                        <label for="prioridad"><b> Prioridad: </b></label>
                    </div>
                    <div class="col-9">
                        <select class="form-control" name="prioridad" id="prioridad" >
                            <option value="3" >Media</option>
                            <option  value="1"> Urgente</option>
                            <option  value="2"> Alta </option>
                            <option  value="4"> Baja</option>
                        <select>
                    </div>
                </div><br>
                <div class="row">
                    <div class="col-3">
                        <b> Equipo: </b>
                    </div>
                    <div class="col-9">
                    <input readonly class="form-control-plaintext" type="text"  name="equipo" id="equipo" value="equipo" style="height:70%;">
                    </div>
                </div><br>
                <div class="row">
                    <div class="col-3">
                        <b> Tipo: </b>
                    </div>
                    <div class="col-9">
                    <input readonly class="form-control-plaintext" type="text"  name="tipo" id="tipo" value="tipo" style="height:70%;">
                   
                    </div>
                </div><br>
                <div class="row">
                    <div class="col-3">
                        <b> Categoria: </b>
                    </div>
                    <div class="col-9">
                    <input readonly class="form-control-plaintext" type="text"  name="categoria" id="categoria"  style="height:70%;">
                   
                    </div>
                </div><br>
                <div class="row">
                    <div class="col-3">
                        <b> Subcategoria: </b>
                    </div>
                    <div class="col-9">
                    <input readonly class="form-control-plaintext" type="text"  name="subcategoria" id="subcategoria"  style="height:70%;">
                   
                    </div>
                </div><br>
                <div class="row">
                    <div class="col-3">
                        <b> Descripción: </b>
                    </div>
                    <div class="col-9">
                    <input readonly class="form-control-plaintext" type="text"  name="descrip" id="descrip" value="descrip" style="height:70%;">
                  
                    </div>
                </div><br>
                <div class="row">
                    <div class="col-3">
                        <b> Fecha: </b>
                    </div>
                    <div class="col-9">
                    <input readonly class="form-control-plaintext" type="fecha"  name="fecha" id="fecha" value="fecha" style="height:70%;">
                  
                    </div>
                </div><br>


                <div class="row">
                    <div class="col-3">
                        <label for="selecturgencia"><b> Encargado: </b></label>
                    </div>
                    <div class="col-9">
                        <select class="form-control" id="selectEncargado" name="selectEncargado">
                            <option value="0">Sin asignar</option>
                                <?php
                                    foreach ($users as $us){
                                            echo "<option value='".$us->usuariorolId."'>".$us->name." ".$us->apellidoP." ".$us->apellidoM."-".$us->rol."</option>";
                                    }
                                ?>
                            </select>
                    </div>                   
                </div>
            </div>
            <div class="modal-footer">
              <button type="submit" class="btn btn-primary"> Aceptar </button>
              <button type="button" class="btn btn-secondary" data-dismiss="modal"> Close </button>
              </form>
            </div>
          </div>
        </div>
    </div>
  


    
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script type="text/javascript" src="../../../public/js/jquery-3.4.1.min.js"></script>


<script type="text/javascript">
                        var ATTRIBUTES = ['ticket','name','equipo','tipo','descrip','fecha','prioridad','categoria','subcategoria'];
                        
                     $('[data-toggle="modal"]').on('click', function (e) {

                      // convert target (e.g. the button) to jquery object
                      var $target = $(e.target);
                      // modal targeted by the button
                      var modalSelector = $target.data('target');
                      
                      // iterate over each possible data-* attribute
                      var i=0;
                      ATTRIBUTES.forEach(function (attributeName) {
                        // retrieve the dom element corresponding to current attribute
                        var $modalAttribute = $(modalSelector + ' #modal-' + attributeName);
                        var dataValue = $target.data(attributeName);
                        // if the attribute value is empty, $target.data() will return undefined.
                        // In JS boolean expressions return operands and are not coerced into
                        // booleans. That way is dataValue is undefined, the left part of the following
                        // Boolean expression evaluate to false and the empty string will be returned
                        $modalAttribute.text(dataValue || '');
                        if (dataValue==null) {
                            dataValue="";
                        }
                            document.getElementById(ATTRIBUTES[i]).value =dataValue;
                            
                            i++;
                    });
                    });
                    </script>
                  
  @endsection