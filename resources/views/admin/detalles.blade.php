@extends('licenciasconsulta')
@include('layouts.navbar')
@include('layouts.sidebarAdmin') 
@section('solicitudes-tabla')
<div class="card mb-3">
     <div class="row form-group">
                         <div class="col-12">
                            <label for="nserie_equipo">Usuario: {{$equipo->user->name }}</label> 
                        </div> 
                        <div class="col-12">
                            <label for="nserie_equipo">Departamento: {{$equipo->user->departamento }}</label> 
                        </div> 
                        <div class="col-12">
                            <label for="nserie_equipo">Equipo (Numero de serie activo): {{$equipo->nserie_equipo}}</label> 
                        </div>
                        <div class="col-6">
                            <label for="nserie_equipo">Número de licencias asignadas: {{ $e}}</label> 
                        </div>
             </div>
   </div>
   <div class="card-footer small text-muted">Updated yesterday at 11:59 PM</div>
</div>
{{-- <a class="btn btn-warning" href="{{url('/tickets/')}}" > Regresar</a> --}}
@endsection
