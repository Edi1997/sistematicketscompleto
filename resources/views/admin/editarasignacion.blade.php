@extends('asignacioneditar')
@include('layouts.navbar')
@include('layouts.sidebarAdmin')  
 @section('solicitudes-tabla')

                    <h5 class="modal-title text-center" id="exampleModalLabel"> EDITAR INFORMACIÓN </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form action="{{ url('/equipo/'.$licenciasusuario->id)}}" method="post" enctype="multipart/form-data" >
               {{csrf_field()}}
                {{method_field('PATCH')}}
                    <div class="modal-body">
                        <div class="row form-group">
                            <div class="col-3">
                                <label for="nserie_equipo"> Equipo(Numero de serie activo): </label>
                            </div>
                            <div class="col-9">
                                <input required class="form-control" id="nserie_equipo" name="nserie_equipo" value="{{$licenciasusuario->nserie_equipo}}">
                            </div>
                        </div>
                        
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger" data-dismiss="modal"> Cancelar </button>
                        <button type="submit" class="btn btn-primary"> Aceptar </button>
                    </div>
                </form>
                {{-- <a class="btn btn-warning" href="{{url('/licencia_usuario/')}}" > Regresar</a> --}}
            </div>
             
        </div>
          
                               
                            
    </div>
    @endsection
 
