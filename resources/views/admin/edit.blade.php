@extends('usuarios')
@include('layouts.app')

@include('layouts.navbar')
@include('layouts.sidebarAdmin')
@section('content')

                    <h5 class="modal-title text-center" id="exampleModalLabel"> EDITAR INFORMACIÓN </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                </br>
                <form action="{{ url('/usuarios/'.$user->id)}}" method="post" enctype="multipart/form-data" >
               {{csrf_field()}}
                {{method_field('PATCH')}}
                    <div class="modal-body">
                        <div class="row form-group">
                            <div class="col-3">
                                <label for="name"> Nombre: </label>
                            </div>
                            <div class="col-9">
                                <input required class="form-control" id="name" name="name" value="{{$user->name}}">
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col-3">
                                <label for="apellidoP"> Apellido Paterno: </label>
                            </div>
                            <div class="col-9">
                                <input required required class="form-control" id="apellidoP" name="apellidoP" value="{{$user->apellidoP}}">
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col-3">
                                <label for="apellidoM">Apellido Materno: </label>
                            </div>
                            <div class="col-9">
                                <input required class="form-control" id="apellidoM"  name="apellidoM" value="{{$user->apellidoM}}">
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col-3">
                                <label for="email"> Correo: </label>
                            </div>
                            <div class="col-9">
                                <input required class="form-control" id="email" name="email" value="{{$user->email}}">
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col-3">
                                <label for="edificio"> Edificio: </label>
                            </div>
                            <div class="col-9">
                                <input required class="form-control" id="edificio" name="edificio" value="{{$user->edificio}}">
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col-3">
                                <label for="departamento"> Departamento: </label>
                            </div>
                            <div class="col-9">
                                <input required class="form-control" id="departamento" name="departamento" value="{{$user->departamento}}">
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col-3">
                                <label for="extensión"> Extensión: </label>
                            </div>
                            <div class="col-9">
                                <input required class="form-control" id="extensión" name="extension" value="{{$user->extension}}">
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col-3">
                                <label for="tipo"> Tipo: </label>
                            </div>
                            <div class="col-9">
                                <select class="form-control" id="tipo" name="idRol" value="3" selected="4">
                                    <?php

                                        foreach ($roles as $rol) {
                                            if ($rol->id==$usuarioRol[0]->idRol) {
                                                echo '<option selected value="'.$rol->id.'">'.$rol->rol.' </option>';                
                                            }else{
                                                echo '<option value="'.$rol->id.'">'.$rol->rol.' </option>';                
                                            }
                                        }
                                    ?>
                                
                                    <!-- <option value="5"> Usuario </option>
                                    <option value="2"> HelpDesk </option>
                                    <option value="3"> Agente 1° Nivel </option>
                                    <option value="4"> Agente 2° Nivel </option> -->
                                    
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger" data-dismiss="modal"> Cancelar </button>
                        <button type="submit" class="btn btn-primary"> Aceptar </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    @endsection

