@extends('editmantenimiento')
@include('layouts.navbar')
@include('layouts.sidebarAdmin') 
@section('solicitudes-tabla')
<div class="card mb-3">
 <div class="card-header">
        <i class="fas fa-table"></i>
       Editar solicitud de mantenimiento
    </div>
    <div class="card-body">
 <div class="table-responsive">
 <form action="{{ url('/mantenimiento/'.$mantenimiento->id)}}" method="post" enctype="multipart/form-data" >
               {{csrf_field()}}
                {{method_field('PATCH')}}
                    <div class="form-group">
                    {{-- fecha --}}
                 <label for="fecha" name="fecha" id="fecha" > Fecha: </label>
                 <input  type="date" class="form-control" name="fecha" id="fecha" required  value="{{$mantenimiento->fecha}}">
                </div>  
  {{-- área--}}
         <label  > Área Solicitante : (Escribe el área solicitante o despliega las opciones (Departamentos))</label><br>
<input list="area" name="area" placeholder="Doble clic menú desplegable de opciones (Departamentos)" class="form-control" style="height: 50px; width: 460px;" value="{{$mantenimiento->area}}" required="">
<datalist id="area">
  <option value="Ciencias Económico - Administrativo">Ciencias Económico - Administrativo</option>
                  <option value="DEPI">DEPI</option>
                  <option value="Eléctrica">Eléctrica</option>
                  <option value="Gestión Empresarial">Gestión Empresarial</option>
                  <option value="Ingeniería Bioquimica">Ingeniería Bioquimica</option>
                  <option value="Ingeniería Industrial">Ingeniería Industrial</option>
                  <option value="Ingeniería en Materiales">Ingeniería en Materiales</option>
                  <option value="Ingeniería Mécanica">Ingeniería Mécanica</option>
                  <option value="Ingeniería Mecatrónica">Ingeniería Mecatrónica</option>
                  <option value="Posgrado de Eléctrica">Posgrado de Eléctrica</option>
                  <option value="Sistemas y Computación">Sistemas y Computación</option>
</datalist>
 {{-- Responsable del area--}}
 <label  > Responsable del Área: </label><br>
<input list="responsable" name="responsable" placeholder="Centro de cómputo o categoría" class="form-control" style="height: 50px;  width: 460px;" value="{{$mantenimiento->responsable}}" required="">
<datalist id="responsable">
  <option value="Centro de Cómputo">Centro de Cómputo</option>
</datalist>
 

{{-- Tipo de servicio--}}
  <label > Tipo de servicio </label><br>
  <div class="container">
  <div class="row">
    <div class="col">
      <label style="margin-left: 80px;" > Recursos Materiales y Servicios: </label><br>
                   <p>
                     <input id = "tiposervicio" type = "radio" name = "tiposervicio"
                        value = "Limpieza" @if(($mantenimiento->tiposervicio)=="Limpieza") checked @endif />
                     <label for = "tiposervicio">Limpieza</label>

                   <p>
                     <input id = "tiposervicio" type = "radio" name = "tiposervicio"
                        value = "Cerrajería"  @if(($mantenimiento->tiposervicio)=="Cerrajería") checked @endif/>
                     <label for = "tiposervicio">Cerrajería</label>
                  </p>

                   <p>
                     <input id = "tiposervicio" type = "radio" name = "tiposervicio"
                        value = "Evento"  @if(($mantenimiento->tiposervicio)=="Evento") checked @endif/>
                     <label for = "tiposervicio">Evento</label>
                  </p>
                <p>
                 <p>
                     <input id = "tiposervicio" type = "radio" name = "tiposervicio"
                        value = "Mobiliario" @if(($mantenimiento->tiposervicio)=="Mobiliario") checked @endif />
                     <label for = "tiposervicio">Mobiliario</label>
                  </p>
                  <p>
                     <input id = "tiposervicio" type = "radio" name = "tiposervicio"
                        value = "Otro"  @if(($mantenimiento->tiposervicio)=="Otro") checked @endif />
                     <label for = "tiposervicio">Otro</label>
                  </p>

    </div>
    <div class="col">
    <label style="margin-left: 80px;" > Centro de Cómputo: </label><br>
                   <p>
                     <input id = "tiposervicio" type = "radio" name = "tiposervicio"
                        value = "Hardware" @if(($mantenimiento->tiposervicio)=="Hardware") checked @endif />
                     <label for = "tiposervicio">Hardware</label>
                  </p>

                   <p>
                     <input id = "tiposervicio" type = "radio" name = "tiposervicio"
                        value = "Software"  @if(($mantenimiento->tiposervicio)=="Software" ) checked @endif/>
                     <label for = "tiposervicio">Software</label>
                  </p>
              
               <p>
                     <input id = "tiposervicio" type = "radio" name = "tiposervicio"
                        value = "Telefónia"  @if(($mantenimiento->tiposervicio)=="Telefónia") checked @endif/>
                     <label for = "tiposervicio">Telefónia</label>
                  </p>
                  <p>
                     <input id = "tiposervicio" type = "radio" name = "tiposervicio"
                        value = "Internet"  @if(($mantenimiento->tiposervicio)=="Internet") checked @endif/>
                     <label for = "tiposervicio">Internet</label>
                  </p>
                  <p>
                     <input id = "tiposervicio" type = "radio" name = "tiposervicio"
                        value = "Redes" @if(($mantenimiento->tiposervicio)=="Redes") checked @endif />
                     <label for = "tiposervicio">Redes</label>
                  </p>
                  <p>
                     <input id = "tiposervicio" type = "radio" name = "tiposervicio"
                        value = "Otro" @if(($mantenimiento->tiposervicio)== "Otro") checked @endif />
                     <label for = "tiposervicio">Otro</label>
                  </p>
                  
    </div>
  </div>

{{-- Descripción--}}
 <div class="row form-group">
                        <div style="margin-left: 30px;">
                            <label for="descripcion"> Descripción del servicio que solicita o falla a reparar: </label>
                        </div>
                         <br>
                        <div class="col-9" >
                            <textarea type="text" class="form-control" id="descripcion" name="descripcion" required="" style="height: 68px; width: 366px; margin-left: 30px;"> {{$mantenimiento->descripcion}}</textarea>
                        </div>
                    </div>

{{-- Trabajo o servicio realizado--}}
 <label for="serviciorealizado" style="margin-left: 30px;"> Trabajo o servicio realizado: </label>
 <div class="row form-group">
                        <br>
                        <div class="col-9">
                            <textarea type="text" class="form-control" id="serviciorealizado" name="serviciorealizado" required="" style="height: 68px; width: 366px; margin-left: 30px;" > {{$mantenimiento->serviciorealizado}}</textarea>
                        </div>
                    </div>


{{-- Material--}}
 <label  style="margin-left: 30px;" for="material"> Material Utilizado: </label>
 <div class="row form-group">
                      
                         <br>
                        <div class="col-9">
                            {{-- <input type="text" class="form-control" id="version" name="version" required=""> --}}
                            <textarea type="text" class="form-control" id="material" name="material" required=""style="height: 68px; width: 366px; margin-left: 30px;" > {{$mantenimiento->material}}</textarea>
                        </div>
                    </div>
{{-- Solicita--}}
<div class="row form-group">
                        <div class="col-3">
                            <label for="solicitante"> Solicita (Nombre): </label>
                        </div>
                        <div class="col-9">
                            <input class="form-control" id="solicitante" name="solicitante" value="{{$mantenimiento->solicitante}}" required="">
                        </div>
                    </div>

{{-- Atendio--}}
  <div class="row form-group">
                        <div class="col-3">
                            <label for="realizador"> Atendió (Nombre): </label>
                        </div>
                        <div class="col-9">
                            <input class="form-control" id="realizador" name="realizador" value="{{$mantenimiento->realizador}}" required="">
                        </div>
                    </div>
{{-- Vo.Bo--}}

                     <div class="row form-group">
                        <div class="col-3">
                            <label for="recibidor"> Vo.Bo (Nombre)</label>
                        </div>
                        <div class="col-9">
                            <input class="form-control" id="recibidor" name="recibidor" value="{{$mantenimiento->recibidor}}" required="">
                        </div>
                    </div>

                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary"> Aceptar </button>
                        
                    </div>
                   
                </form>
               
      </div>
    </div>
</div>
@endsection