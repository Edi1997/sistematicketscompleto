@extends('licenciass')
@include('layouts.navbar')
@include('layouts.sidebarAdmin')  
@section('solicitudes-tabla')
<div class="card mb-3">
    <div class="card-header">
        <i class="fas fa-table"></i>
        Listado de licencias
    </div>
    <div class="card-body">
        <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                <thead>
                    <tr>

                        <th> Nombre del programa </th>
                        <th> Licencia Digitos</th>
                        <th> Versión del Programa</th>
                        <th> Subversión </th>
                        <th> Opciones </th>
                        <th></th>
                    </tr>
                </thead>
               <tbody>
               @foreach($licencias as $licencia)
                   <tr>
                    <td>{{ $licencia ->nombre_programa }}</td>
                    <td>{{ $licencia ->digitos}}</td>
                    <td>{{ $licencia->version }}</td>
                    <td>{{ $licencia->subversion }}</td>
                    <td> <a class="btn btn-warning" href="{{url('/altalicencias/'.$licencia->id.'/edit')}}" class="secondary-content"><i class="fa fa-edit"></i></a></a></td>
                    <td>
                         <form method="post" action="{{url('/altalicencias/'.$licencia->id)}}">
                               {{csrf_field()}}
                               {{method_field('DELETE')}}

                               <button   class="btn btn-danger" type="submit" onclick="return confirm('¿Borrar?');"><i class="fa fa-trash"></i></button>                 
                           </form>
                    </td>
                   </tr>
               @endforeach
               </tbody>
           </table>
       </div>
   </div>
   <div class="card-footer small text-muted">Updated yesterday at 11:59 PM</div>
</div>
 {{-- <a class="btn btn-warning" href="{{url('/tickets/')}}" > Regresar</a> --}}
@endsection



@section('modalregistro')
<div class="modal fade" id="modalregistro" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header text-center">
                <h5 class="modal-title text-center" id="exampleModalLabel"> AGREGAR LICENCIA </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>


            <form action="{{route('altalicencias.store')}}" method="post" >
                {{csrf_field()}}
                <div class="modal-body">
                    <div class="row form-group">
                        <div class="col-3">
                            <label for="nombre_programa"> Nombre del programa: </label>
                        </div>
                        <div class="col-9">
                            <input class="form-control" id="nombre_programa" name="nombre_programa" required="">
                        </div>
                    </div>

                    <div class="row form-group">
                        <div class="col-3">
                            <label for="digitos"> Ultimos 5 digitos de la Licencia: </label>
                        </div>
                        <div class="col-9">
                            <input class="form-control" id="digitos" name="digitos" required="">
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col-3">
                            <label for="version"> Versión: </label>
                        </div>
                        <div class="col-9">
                            <input class="form-control" id="version" name="version" required="">
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col-3">
                            <label for="subversion"> Subversión (Solo si aplica): </label>
                        </div>
                        <div class="col-9">
                            <input class="form-control" id="subversion" name="subversion" >
                        </div>
                    </div>
             
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal"> Cancelar </button>
                <button type="submit" class="btn btn-primary"> Aceptar </button>
            </div>
        </form>
    </div>
    {{-- <a class="btn btn-warning" href="{{url('/tickets/')}}" > Regresar</a> --}}
</div>
</div>
@endsection






