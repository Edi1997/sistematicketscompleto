@extends('correo')
@include('layouts.navbar')
@include('layouts.sidebarAdmin') 
@section('solicitudes-tabla')
 
   <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">     
<div class="card mb-3">

    <div class="card-header">
        <i class="fas fa-table"></i>
        Datos de peticiones (No validadas)
    </div>
    <div class="card-body">
        <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                <thead>
                    <tr>
                    <th> Validar</th>
                        <th> Fotografia (acercar cursor para zoom)</th>
                         <th> Estatus</th>
                        <th> Cambiar estatus </th>
                        <th> Numero de Control</th>
                        <th> Carrera</th>
                        <th> Nombre</th>
                        <th> Apellido Paterno</th>
                         <th>Apellido Materno</th> 
                        <th> Peticion</th>
                        <th> Descripción</th>
                        <th> Email</th>
                        <th> Numero de atencion</th>
                        <th> Fecha de Creación</th>
                        <th>Opciones </th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>

              <form method="POST" action="{{route('correos.store')}}">
                    @csrf 
                  
                  @foreach($datosNoValidados as $datosNoValidado )
                  <tr>

                   <td>
                   <input type="checkbox"  name="validados[]" id="{{$datosNoValidado}}" value="{{ $datosNoValidado->numerocontrol}},{{ $datosNoValidado->carrera}},{{ $datosNoValidado->apellidoPaterno}},{{ $datosNoValidado->apellidoMaterno}},{{ $datosNoValidado->nombre}},{{ $datosNoValidado->opcion}},{{ $datosNoValidado->descripcion}}"> 
                
                    </td> 

                     <td>
                     <img src="{{ asset('storage').'/'.$datosNoValidado->foto}}" style="width:60%;cursor:zoom-in" onclick="onClick(this)">
                     <div id="modal01" class="w3-modal" onclick="this.style.display='none'">
                     <span class="w3-button w3-hover-red w3-xlarge w3-display-topright">&times;</span>
                     <div class="w3-modal-content w3-animate-zoom">
  <img class="w3-modal-content" id="img01"style="width:100%">
  </div>
</div> 
                       </td> 
                        <td>{{ $datosNoValidado->estatus}}</td> 
                     <td> <a class=" btn btn-success" href="{{route('correos.show',$datosNoValidado->id)}}" class="secondary-content"><svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-arrow-clockwise" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
  <path fill-rule="evenodd" d="M8 3a5 5 0 1 0 4.546 2.914.5.5 0 0 1 .908-.417A6 6 0 1 1 8 2v1z"/>
  <path d="M8 4.466V.534a.25.25 0 0 1 .41-.192l2.36 1.966c.12.1.12.284 0 .384L8.41 4.658A.25.25 0 0 1 8 4.466z"/>
</svg></a></a>
                   </td>
                    <td>{{ $datosNoValidado->numerocontrol}}</td>
                    <td>{{ $datosNoValidado->carrera}}</td>
                    <td>{{ $datosNoValidado->nombre}}</td>
                    <td>{{ $datosNoValidado->apellidoPaterno}}</td>
                    <td>{{ $datosNoValidado->apellidoMaterno}}</td>
                    <td>{{ $datosNoValidado->opcion}}</td>
                    <td>{{ $datosNoValidado->descripcion}}</td>
                    <td>{{ $datosNoValidado->email}}</td>
                    <td>{{ $datosNoValidado->order_nr}}</td> 
                    <td>{{ $datosNoValidado->created_at}}</td> 
                    <td> 
                     <a class="btn btn-warning" href="{{url('/correos/'.$datosNoValidado->id.'/edit')}}" class="secondary-content"><i class="fa fa-edit"></i></a></a>
                       <a href="{{route('correos.destroy',$datosNoValidado->id)}}" class="btn btn-danger"onclick="return confirm('¿Borrar?');"><i class="fa fa-trash"></i></a> 
                    </td>
       
                   </tr>
                     @endforeach
   </tbody> 
     
      </table>   
           <button type="submit" class="btn btn-info" >Exportar a CSV validados</button>    
             </form>    
            <br>  <br><a  href="{{ route('exportarcompleta') }}"  class="btn btn btn-secondary" style="width: 300px">Exportar a CSV tabla completa</a>     
           {{-- <img src=" {{ asset('storage').'/'.$datosNoValidado->foto}}" onmouseover="this.width=1000;this.heigh=1000;"onmouseout="this.width=50;this.heigh=50;"alt="" width="50">  --}}
                    {{-- <img src=" {{ asset('storage').'/'.$datosNoValidado->foto}}" alt="" width="400"> --}}
              
       </div>
   </div>
   <div class="card-footer small text-muted">Updated yesterday at 11:59 PM</div>
</div>
   
{{-- <a class="btn btn-warning" href="{{url('/tickets/')}}" > Regresar</a> --}}
<script>
function onClick(element) {
  document.getElementById("img01").src = element.src;
  document.getElementById("modal01").style.display = "block";
}
</script>

   

@endsection

