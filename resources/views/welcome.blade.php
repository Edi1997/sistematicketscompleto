@extends('layouts.logadmin')
@section('content')
  

  <div id="wrapper">

    <!-- Sidebar -->
    <ul class="sidebar navbar-nav">
      <li class="nav-item active">
        <a class="nav-link" href="index.html">
          <i class="fas fa-fw fa-tachometer-alt"></i>
          <span> Inicio </span>
        </a>
      </li>
      <!-- <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="pagesDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          <i class="fas fa-fw fa-folder"></i>
          <span> Tickets </span>
        </a>
        <div class="dropdown-menu" aria-labelledby="pagesDropdown">
          <h6 class="dropdown-header">Login Screens:</h6>
          <a class="dropdown-item" href="login.html">Login</a>
          <a class="dropdown-item" href="register.html">Register</a>
          <a class="dropdown-item" href="forgot-password.html">Forgot Password</a>
          <div class="dropdown-divider"></div>
          <h6 class="dropdown-header">Other Pages:</h6>
          <a class="dropdown-item" href="404.html">404 Page</a>
          <a class="dropdown-item" href="blank.html">Blank Page</a>
        </div>
      </li> -->
      <li class="nav-item">
        <a class="nav-link" href="asignaciones.html">
          <i class="fas fa-fw fa-chart-area"></i>
          <span> Asignaciones </span></a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="ticket.html">
          <i class="fas fa-fw fa-list"></i>
          <span> Generar Ticket </span></a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="usuarios.html">
          <i class="fas fa-fw fa-table"></i>
          <span> Usuarios </span></a>
      </li>
    </ul>

    <div id="content-wrapper">

      <div class="container-fluid">

        <!-- Breadcrumbs-->
        <ol class="breadcrumb">
          <li class="breadcrumb-item">
            <a href="#">Gestion de tickets</a>
          </li>
          <li class="breadcrumb-item active"></li>
        </ol>

        <!-- Icon Cards-->
        <div class="row">
          <div class="col-xl-3 col-sm-6 mb-3">
            <div class="card text-white bg-secondary o-hidden h-100">
              <div class="card-body">
                <div class="card-body-icon">
                  <i class="fas fa-fw fa-comments"></i>
                </div>
                <div class="mr-5"> # Solicitudes Nuevas </div>
              </div>
              <a class="card-footer text-white clearfix small z-1" href="#">
                <span class="float-left"> Ver </span>
                <span class="float-right">
                  <i class="fas fa-angle-right"></i>
                </span>
              </a>
            </div>
          </div>
          <div class="col-xl-3 col-sm-6 mb-3">
            <div class="card text-white bg-danger o-hidden h-100">
              <div class="card-body">
                <div class="card-body-icon">
                  <i class="fas fa-fw fa-list"></i>
                </div>
                <div class="mr-5"> # Urgentes </div>
              </div>
              <a class="card-footer text-white clearfix small z-1" href="#">
                <span class="float-left"> Ver </span>
                <span class="float-right">
                  <i class="fas fa-angle-right"></i>
                </span>
              </a>
            </div>
          </div>
          <div class="col-xl-3 col-sm-6 mb-3">
            <div class="card text-white o-hidden h-100" style="background-color: orange;">
              <div class="card-body">
                <div class="card-body-icon">
                  <i class="fas fa-fw fa-list"></i>
                </div>
                <div class="mr-5"> # Prioridad Alta</div>
              </div>
              <a class="card-footer text-white clearfix small z-1" href="#">
                <span class="float-left"> Ver </span>
                <span class="float-right">
                  <i class="fas fa-angle-right"></i>
                </span>
              </a>
            </div>
          </div>
          <div class="col-xl-3 col-sm-6 mb-3">
            <div class="card text-white o-hidden h-100" style="background-color: rgb(233, 230, 60);">
              <div class="card-body">
                <div class="card-body-icon">
                  <i class="fas fa-fw fa-list"></i>
                </div>
                <div class="mr-5"> # Prioridad Media </div>
              </div>
              <a class="card-footer text-white clearfix small z-1" href="#">
                <span class="float-left"> Ver </span>
                <span class="float-right">
                  <i class="fas fa-angle-right"></i>
                </span>
              </a>
            </div>
          </div>
          <div class="col-xl-3 col-sm-6 mb-3">
            <div class="card text-white bg-primary o-hidden h-100">
              <div class="card-body">
                <div class="card-body-icon">
                  <i class="fas fa-fw fa-list"></i>
                </div>
                <div class="mr-5"> # Prioridad Baja </div>
              </div>
              <a class="card-footer text-white clearfix small z-1" href="#">
                <span class="float-left"> Ver </span>
                <span class="float-right">
                  <i class="fas fa-angle-right"></i>
                </span>
              </a>
            </div>
          </div>
          <div class="col-xl-3 col-sm-6 mb-3">
            <div class="card text-white bg-success o-hidden h-100">
              <div class="card-body">
                <div class="card-body-icon">
                  <i class="fas fa-fw fa-list"></i>
                </div>
                <div class="mr-5"> Finalizados </div>
              </div>
              <a class="card-footer text-white clearfix small z-1" href="#">
                <span class="float-left"> Ver </span>
                <span class="float-right">
                  <i class="fas fa-angle-right"></i>
                </span>
              </a>
            </div>
          </div>
        </div>

        <!-- Area Chart Example
        <div class="card mb-3">
          <div class="card-header">
            <i class="fas fa-chart-area"></i>
            Area Chart Example</div>
          <div class="card-body">
            <canvas id="myAreaChart" width="100%" height="30"></canvas>
          </div>
          <div class="card-footer small text-muted">Updated yesterday at 11:59 PM</div>
        </div> -->

        <!-- DataTables Example -->
        <div class="card mb-3">
          <div class="card-header">
            <i class="fas fa-table"></i>
            Tickets </div>
          <div class="card-body">
            <div class="table-responsive">
              <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                <thead>
                  <tr>
                    <th> Solicitante </th>
                    <th> Prioridad </th>
                    <th> Estado </th>
                    <th> Equipo </th>
                    <th> Tipo </th>
                    <th> Fecha de recepción </th>
                    <th> Fecha de finalización </th>
                    <th> Encargado </th>
                  </tr>
                </thead>
                <tfoot>
                  <tr>
                    <th> Solicitante </th>
                    <th> Prioridad </th>
                    <th> Estado </th>
                    <th> Equipo </th>
                    <th> Tipo </th>
                    <th> Fecha de recepción </th>
                    <th> Fecha de finalización </th>
                    <th> Encargado </th>
                  </tr>
                </tfoot>
                <tbody>
                  <tr>
                    <td> Juan Carlos Olivares </td>
                    <td style="background-color: orange;"> Alta </td>
                    <td> En proceso </td>
                    <td> Teléfono </td>
                    <td> Falla </td>
                    <td> 12 / 11 / 2019 </td>
                    <td>  </td>
                    <td> Germán Juárez </td>
                  </tr>
                  <tr>
                    <td> Yolanda Garcia </td>
                    <td class="bg-danger"> Urgente </td>
                    <td> En proceso </td>
                    <td> Computador </td>
                    <td> Falla </td>
                    <td> 14 / 11 / 2019 </td>
                    <td>  </td>
                    <td> Sin asignar </td>
                  </tr>
                  <tr>
                    <td> Benito Sanchez </td>
                    <td class="bg-primary"> Baja </td>
                    <td> Finalizado </td>
                    <td> PC </td>
                    <td> Otro </td>
                    <td> 23 / 10 / 2019 </td>
                    <td> 25 / 11 / 2019 </td>
                    <td> Jesus Romero </td>
                  </tr>
                  <tr>
                    <td> Alejandro Naranjo </td>
                    <td style="background-color: rgb(233, 230, 60);"> Media </td>
                    <td> Finalizado </td>
                    <td> Telefono </td>
                    <td> Otro </td>
                    <td> 11 / 11 / 2019 </td>
                    <td> 14 / 11 / 2019 </td>
                    <td> Jose Hernandez </td>
                  </tr>
                </tbody>
              </table>
            </div>
          </div>
          <div class="card-footer small text-muted"></div>
        </div>
      </div>
      <!-- /.container-fluid -->

      <!-- Sticky Footer -->
      <footer class="sticky-footer">
        <div class="container my-auto">
          <div class="copyright text-center my-auto">
            <span>Copyright © Your Website 2019</span>
          </div>
        </div>
      </footer>

    </div>
    <!-- /.content-wrapper -->

  </div>
  <!-- /#wrapper -->

  <!-- Scroll to Top Button-->
  <a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
  </a>

  <!-- Logout Modal-->
  <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
          <button class="close" type="button" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>
        <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
        <div class="modal-footer">
          <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
          <a class="btn btn-primary" href="login.html">Logout</a>
        </div>
      </div>
    </div>
  </div>

  <!-- Bootstrap core JavaScript-->
  <script src="vendor/jquery/jquery.min.js"></script>
  <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- Core plugin JavaScript-->
  <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

  <!-- Page level plugin JavaScript-->
  <script src="vendor/chart.js/Chart.min.js"></script>
  <script src="vendor/datatables/jquery.dataTables.js"></script>
  <script src="vendor/datatables/dataTables.bootstrap4.js"></script>

  <!-- Custom scripts for all pages-->
  <script src="js/sb-admin.min.js"></script>

  <!-- Demo scripts for this page-->
  <script src="js/demo/datatables-demo.js"></script>
  <script src="js/demo/chart-area-demo.js"></script>

  <script src="js/fullcalendar.min.js"></script>
  <!-- Full Google Calendar - Calendar -->
  <script src="assets/fullcalendar/fullcalendar/fullcalendar.js"></script>
  <!--script for this page only-->
  <script src="js/calendar-custom.js"></script>
  <script src="js/jquery.rateit.min.js"></script>
  <!-- custom select -->
  <script src="js/jquery.customSelect.min.js"></script>
  <script src="assets/chart-master/Chart.js"></script>
  <link href="assets/fullcalendar/fullcalendar/bootstrap-fullcalendar.css" rel="stylesheet" />
  <link href="assets/fullcalendar/fullcalendar/fullcalendar.css" rel="stylesheet" />
  <link rel="stylesheet" href="css/fullcalendar.css">
</body>

</html>

@endsection
