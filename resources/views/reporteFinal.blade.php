@include('layouts.scripts')
@include('layouts.footer')
@include('layouts.breadcrumb')

<!DOCTYPE html>
    <html lang="en">
        <head>
            @include('layouts.head')
        </head>
        <body id="page-top">
            {{-- NAVBAR --}}
            @yield('navbar')
            @yield('logout')
            {{-- CONTENT --}}
            <div id="wrapper">
                @yield('sidebar')
                <div id="content-wrapper">
                    <div class="container-fluid">
                        @yield('bread-reporteFinal')
                        <div class="text-center">
                        </div><br>
                        @yield('reporteFinal')
                    </div>
                    @yield('footer')
                </div>
            </div>
            @yield('top')
        </body>
    </html>
    @yield('scripts')