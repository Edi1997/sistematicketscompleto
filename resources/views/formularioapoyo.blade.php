@include('layouts.app')
@extends('registro')

@section('registro')
<div class="container" style="margin-top:2px;">
  <div class="card card-register mx-auto " >
    <div class="card-header"> Apoyo Correo Electrónico </div>
    <div class="card-body">
    {{-- Validacion Mensajes --}}
    @if (count($errors) > 0)
    <div class="alert alert-danger">
    	<p>Corrige los siguientes errores:</p>
        <ul>
            @foreach ($errors->all() as $message)
                <li>{{ $message }}</li>
            @endforeach
        </ul>
    </div>
@endif
        <form method="POST" class="form-horizontal" enctype="multipart/form-data" action="{{route('apoyo.store')}}">
        @csrf
          <div class="form-group">
          <div class="form-row">
            <div class="col-md-6">
              <div class="form-label-group">
                <input type="text" id="numerocontrol" class="form-control" placeholder="Numero de control" required="required" name="numerocontrol">
                <label for="numerocontrol"> Número de control</label>
              </div>
            </div>
            
           <div class="col-md-6">
              <div class="form-label-group">
                <input type="text" id="carrera" class="form-control" placeholder="Carrera" required="required" name="carrera">
                <label for="carrera"> Carrera</label>
              </div>
            </div>
          
          </div>
        </div> 
     

         {{-- <div class="form-group">
          <div class="form-label-group">
            <input type="text" id="nombre" class="form-control" placeholder="Nombre" required="required" name="nombre">
            <label for="nombre">Nombre</label>
          </div>
        </div> --}}
         <div class="form-group">
          <div class="form-row">
            <div class="col-md-6">
          
              <div class="form-label-group">
                 
                <input type="text" id="apellidoPaterno" class="form-control" placeholder="Apellido Paterno" required="required" name="apellidoPaterno">
                <label for="apellidoPaterno"> Apellido Paterno </label>
              </div>
            </div>
            <div class="col-md-6">
             
              <div class="form-label-group">
             
                <input type="text" id="apellidoMaterno" class="form-control" placeholder="Apellido Materno" required="required" name="apellidoMaterno">
               <label for="apellidoMaterno"> Apellido Materno</label>
              </div>
            </div>
          </div>
        </div>
 <div class="form-group">
          <div class="form-label-group">
            <input type="text" id="nombre" class="form-control" placeholder="Nombre" required="required" name="nombre">
            <label for="nombre">Nombre(s)</label>
          </div>
        </div>
              <div class="form-group">
                        <div class="form-label-group">
                          <div class="form-group">
                  <select class="form-control" id="opcion" name="opcion"  required>
                    <option value="Correo Nuevo">Correo Nuevo</option>
                    <option value="Falla con el Correo">Falla con el Correo</option>
                    <option value="Restablecer Contraseña">Restablecer Contraseña</option>
                  </select>
                   
                </div>
          </div>
        </div>

 <div class="form-group">
    {{-- <label for="descripcion">Descripción del Problema</label> --}}
          <div class="form-label-group">
            <textarea type="text" class="form-control" id="descripcion" placeholder="Descripción del problema" name="descripcion" required="" ></textarea>
         {{-- <label for="descripcion">Descripción del Problema</label> --}}
          </div>
        </div>
        <div class="form-label-group">
          <input type="email" id="email" class="form-control" placeholder="Ej.: usuario@servidor.com" required="required" name="email">
          <label for="email"> Email de Contacto, Ej.: usuario@servidor.com</label>
        </div>
        
<div class="form-group">

          <div class="form-label-group">
          <label for="file">Subir foto de credencial:(Tipos de archivo permitidos: JPG,JPEG,PNG)</label>
          <br>
                <input type="file" name="foto" id="foto" >
               
          </div>
        </div>
{{-- Captcha --}}
         <div class="form-group{{ $errors->has('captcha') ? ' has-error' : '' }}">

                      {{-- <label for="captcha" class="col-md-4 control-label">Captcha</label> --}}


                      <div class="col-md-6">

                          <div class="captcha">

                          <span>{!! captcha_img('flat') !!}</span>

                           <button type="button" class="btn btn-success btn-refresh">Refresh</button>

                          </div>

                          <input id="captcha" type="text" class="form-control mt-2 " placeholder="Enter Captcha" name="captcha">


                          @if ($errors->has('captcha'))

                              <span class="help-block">

                                  <strong>{{ $errors->first('captcha') }}</strong>

                              </span>

                          @endif

                      </div>

                  </div>
        
        {{-- <button type="submit" class="btn btn-primary btn-block">
          {{ __('Register') }}
        </button> --}}
         <button type="submit" class="btn btn-primary btn-block"> Enviar</button>
      </form>
     
    </div>
  </div>
</div>
 <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script>
    $('.btn-refresh').click(function(){
        $.ajax({
            type:'GET',
            url: '{{ url('/refresh_captcha') }}',
            success: function(data){
                    $('.captcha span').html(data);
            }
        });
    });
    </script>

  

@endsection
