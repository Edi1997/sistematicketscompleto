@extends('licenciasconsulta')
@include('layouts.navbar')
 {{-- @include('layouts.sidebarAdmin')  --}}
@section('solicitudes-tabla')

<div class="card mb-3">
    <div class="card-header">
        <i class="fas fa-table"></i>
        Listado de licencias asignadas
    </div>
    <div class="card-body">
        <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                <thead>
                    <tr>
                      <th> Nombre del programa </th>
                        <th> Licencia Digitos</th>
                        <th> Usuario</th>
                        <th> Departamento</th>
                        <th> Equipo (Numero de serie activo)</th>
                        <th> Licencias asignadas</th>
                        <th> Opciones </th>
                        
                    </tr>
                </thead>
               <tbody>
               @foreach($licenciasUsuario as $licenciausuario)
                   <tr>
                    <td>{{ $licenciausuario ->licencia->nombre_programa}}</td>
                    <td>{{ $licenciausuario ->licencia->digitos}}</td>
                    <td>{{ $licenciausuario ->user->name}}</td>
                    <td>{{ $licenciausuario ->user->departamento}}</td>
                    <td>{{ $licenciausuario ->nserie_equipo}}</td>
                    <td>{{ $licenciausuario ->nlicencias_asignadas}}</td>
                    <td>
                        <a class="btn btn-warning" href="{{url('/licencia_usuario/'.$licenciausuario->id.'/edit')}}" class="secondary-content"><i class="fa fa-edit"></i></a></a>
                         <form method="post" action="{{url('/licencia_usuario/'.$licenciausuario->id)}}">
                               {{csrf_field()}}
                               {{method_field('DELETE')}}

                               <button   class="btn btn-danger" type="submit" onclick="return confirm('¿Borrar?');"><i class="fa fa-trash"></i></button>                 
                           </form>
                   </td>
                   </tr>
               @endforeach
               </tbody> 
           </table>
       </div>
   </div>
   <div class="card-footer small text-muted">Updated yesterday at 11:59 PM</div>
</div>
<a class="btn btn-warning" href="{{url('/tickets/')}}" > Regresar</a>
@endsection
