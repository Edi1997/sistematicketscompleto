@extends('licenciasasignacion')
@include('layouts.navbar')
{{-- @include('layouts.sidebarAdmin') --}}
@section('solicitudes-tabla')
<div class="card mb-3">
<h5>Asignando licencia al usuario: {{$user->name}} del departamento {{$user->departamento}}</h5>
<h5>Licencias Diponibles: </h5>
<br>
<form style="margin-left:180px" class="col s12" method="post" action="{{ route('licencia_usuario.store',$user->id )}}">
 @csrf
 @foreach($licencias as $licencia )
     <p>
      <input type="checkbox" name="licencias[]" value="{{ $licencia->id }}" class="filled-in" id="{{ $licencia->id }}"@if($user->has_licencia($licencia->id)) checked @endif />
      <label for= "{{ $licencia->id }}">
      <span>{{ $licencia->nombre_programa }}</span>
       <span>{{ $licencia->version }}</span>
       <span>{{ $licencia->subversion }}</span>
      </label>
     </p>
 @endforeach

           <div class="row form-group">
                        <div class="col-6">
                            <label for="nlicencias_asignadas"> Licencias asignadas: </label>
                        </div>
                        <div class="col-9">
                            <input class="form-control" id="nlicencias_asignadas" name="nlicencias_asignadas" >
                        </div>
             </div>
             <div class="row form-group">
                        <div class="col-6">
                            <label for="nserie_equipo"> Equipo (Numero de serie activo): </label>
                        </div>
                        <div class="col-9">
                            <input class="form-control" id="nserie_equipo" name="nserie_equipo" >
                        </div>
             </div>
        <div class="footer">
                <button type="submit" class="btn btn-primary"> Aceptar </button>
                
            </div>
            
        </div>
         <a class="btn btn-warning" href="{{url('/licencia_usuario/create')}}" > Regresar</a>
        </div>
         
 </form>
 
 </div>
@endsection