@extends('licenciasasignacion')
@include('layouts.navbar')
{{-- @include('layouts.sidebarAdmin') --}}
 @section('solicitudes-tabla')

<div class="card mb-3">
    <div class="card-header">
        <i class="fas fa-table"></i>
        Asignación de licencias
    </div>
    <div class="card-body">
        <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                <thead>
                    <tr>
                        <th> Usuario</th>
                        <th> Departamento</th>
                        <th> Asignar Licencia</th>
                    </tr>
                </thead>
               <tbody>
               @foreach($users as $user)
                   <tr>
                    <td>{{ $user->name}}</td>
                    <td>{{ $user->departamento}}</td>
                    <td><a href="{{ url('/licencia_usuario/show/'.$user->id)}}" class="btn btn-success ">Asignar licencia</a>  </td>
                   </tr>
               @endforeach
               </tbody> 
           </table>
       </div>
   </div>
   <div class="card-footer small text-muted">Updated yesterday at 11:59 PM</div>
</div>
<a class="btn btn-warning" href="{{url('/tickets/')}}" > Regresar</a>
@endsection 


