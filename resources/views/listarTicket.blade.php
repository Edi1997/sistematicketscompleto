@extends('index')
@include('layouts.navbar')
@include('layouts.sidebar')

@section('tabla')
            <table class="table table-bordered" id="listar" width="100%" cellspacing="0">
                <thead>
                    <tr>
                        <th> Solicitante </th>
                        <th> Prioridad </th>
                        <th> Estado </th>
                        <th> Equipo </th>
                        <th> Tipo </th>
                        <th> Fecha de recepción </th>
                        <th> Fecha de finalización </th>
                        <th> Encargado </th>
                        <th> Opciones </th>
                    </tr>
                </thead>
                <tfoot>
                    <tr>
                        <th> Solicitante </th>
                        <th> Prioridad </th>
                        <th> Estado </th>
                        <th> Equipo </th>
                        <th> Tipo </th>
                        <th> Fecha de recepción </th>
                        <th> Fecha de finalización </th>
                        <th> Encargado </th>
                        <th> Opciones </th>
                    </tr>
                </tfoot>
                <tbody>
                    <?php
                    
                    foreach ($tickets as $ticket) {
                        echo "<tr>";

                        echo "<td>".$ticket->idUsuarioTicket."</td>";
                        switch ($ticket->nivel) {
                            case 'Urgente':
                            echo '<td class="bg-danger"> '.$ticket->nivel.' </td>';
                            break;
                            case 'Alta':
                            echo '<td style="background-color: orange;"> '.$ticket->nivel.' </td>';
                            break;
                            case 'Media':
                            echo '<td style="background-color: rgb(233, 230, 60);"> '.$ticket->nivel.' </td>';
                            break;
                            case 'Baja':
                            echo '<td class="bg-primary"> '.$ticket->nivel.' </td>';
                            break;
                            default:
                                # code...
                            break;
                        }
                        echo "<td>".$ticket->estatus."</td>";
                        echo "<td>".$ticket->titulo."</td>";
                        echo "<td>".$ticket->categoriaReporte."</td>";
                        echo "<td>".$ticket->created_at."</td>";
                        echo "<td>".$ticket->updated_at."</td>";
                        echo "<td>".$ticket->idUsuarioAsignado."</td>";
                        echo "<td>";
                        echo '<button class="btn btn-warning" data-toggle="modal" data-target="#modaleditar" style="width: 40px;"><i class="fa fa-edit"></i></button>';
                        echo '<button class="btn btn-danger" style="width: 40px;"><i class="fa fa-trash"></i></button>';
                        echo "</td>";
                        echo "</tr>";
                    }
                    ?>


                    
                </tbody>
            </table>
            <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
            <script type="text/javascript">
                $(document).ready(function(){
                    var tabla=$("listar");
                    var route="listarTicket";
                    $.get(route, function(res){
                        $(res).each(function(key,value){
                            tabla.append(value);
                        });
                    });
                });
            </script>