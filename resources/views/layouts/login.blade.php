@extends('layouts.app')
@extends('login')

@section('login')
<div class="container" style="margin-top: 100px;">
    <h1 class="text-center" style="color: white;"> TICKET MANAGER </h1>
    <div class="card card-login mx-auto mt-5">
      <div class="card-header">Login</div>
      <div class="card-body">
        <form method="POST" action="{{ route('login') }}">
          @csrf
          <div class="form-group">
              <div class="form-label-group">
		      <input type="email" id="inputEmail" class="form-control" placeholder="Correo electronico" name="email" value="{{ old('email') }}" required >
              <label for="inputEmail">Correo electronico</label>
            </div>
          </div>
          <div class="form-group">
            <div class="form-label-group">
              <input id="inputPassword" type="password" class="form-control" placeholder="Contraseña" name="password" required>
              <label for="inputPassword">Contraseña</label>
            </div>
          </div>
          <!-- <div class="form-group">
            <div class="checkbox">
              <label>
                <input type="checkbox" value="remember-me">
                Remember Password
              </label>
            </div>
          </div> -->
          <button type="submit" class="btn btn-primary btn-block">
            {{ __('Login') }}
          </button>
        </form>
        <div class="text-center">
          <a class="d-block small mt-3" href="{{url('/registro')}}"> Solicitar acceso </a>
          <a class="d-block small mt-3" href="{{url('/apoyo')}}"> Apoyo Correo Electronico</a>
        </div>
      </div>
    </div>
@endsection
