@section('scripts')
    <!-- Bootstrap core JavaScript-->
    <script src="{{ asset('vendor/jquery/jquery.min.js') }}" type="text/javascript"></script>

		
	<script src="{{ asset('vendor/bootstrap/js/bootstrap.bundle.min.js') }}" type="text/javascript"></script>

    <!-- Core plugin JavaScript-->
    <script src="{{ asset('vendor/jquery-easing/jquery.easing.min.js') }}" type="text/javascript"></script>

    <!-- Page level plugin JavaScript-->
    <script src="{{ asset('vendor/chart.js/Chart.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('vendor/datatables/jquery.dataTables.js') }}" type="text/javascript"></script>
    <script src="{{ asset('vendor/datatables/dataTables.bootstrap4.js') }}" type="text/javascript"></script>

    <!-- Custom scripts for all pages-->
    <script src="{{ asset('js/sb-admin.min.js') }}" type="text/javascript"></script>

    <!-- Demo scripts for this page-->
    <script src="{{ asset('js/demo/datatables-demo.js') }}" type="text/javascript"></script>
    <script src="{{ asset('js/demo/chart-area-demo.js') }}" type="text/javascript"></script>

     <!--script for this page only-->
    <!-- custom select -->
    <script src="{{ asset('js/jquery.customSelect.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/chart-master/Chart.js') }}" type="text/javascript"></script>
    <!-- Ajax script -->
    <script type="text/javascript" src="../../../public/js/jquery-3.4.1.min.js"></script>
    <script type="text/javascript" src="../../../public/vendor/jquery/jquery.js"></script>
    <!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script> -->
@endsection
