@include('layouts.app')
@section('sidebar')
    <ul class="sidebar navbar-nav">
        <li class="nav-item">
            <a class="nav-link" href="{{ url('/tickets') }}">
            <i class="fas fa-fw fa-tachometer-alt"></i>
            <span> Inicio </span>
            </a>
        </li>
        <li class="nav-item dropdown">
            <a class="nav-link" href="{{ url('/solicitudes-tickets') }}">
                <i class="fas fa-fw fa-sticky-note"></i>
                <span> Solicitudes de Tickets </span>
            </a>
            
        </li>
         {{-- solicitud de mantenimiento --}}
         <div class="dropdown-divider"></div>
          <li class="nav-item">
            <a class="nav-link" href="{{ url('/mantenimiento ') }}">
            <i class="fas fa-fw fa-table"></i>
            <span> Solicitud de mantenimiento </span></a>
        </li>
         {{-- Apoyo Correos--}}
         <div class="dropdown-divider"></div>
          <li class="nav-item">
            <a class="nav-link" href="{{ url('/correos') }}">
            <i class="fas fa-fw fa-table"></i>
            <span> Apoyo Correos </span></a>
        </li>
 <div class="dropdown-divider"></div>
         <li class="nav-item">
            <a class="nav-link" href="{{ url('/altalicencias ') }}">
            <i class="fas fa-fw fa-table"></i>
            <span> Alta de Licencias </span></a>
        </li>
          <li class="nav-item">
            <a class="nav-link" href="{{ route('equipo.create') }}">
            <i class="fas fa-fw fa-table"></i>
            <span> Alta Equipos </span></a>
        </li>
         <li class="nav-item">
            <a class="nav-link" href="{{ url('/licencia_equipo') }}">
            <i class="fas fa-fw fa-table"></i>
            <span> Asignación de Licencias </span></a>
        </li>
         <li class="nav-item">
            <a class="nav-link" href="{{ url('/equipo') }}">
            <i class="fas fa-fw fa-table"></i>
            <span> Consulta de licencias </span></a>
        </li> 
    </ul>
@endsection
