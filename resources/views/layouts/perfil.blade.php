@include('layouts.app')
@extends('perfil')
@include('layouts.navbar')
@include('layouts.sidebarEditar')

@section('perfil')
    <div class="text-center">
        <h1> EDITAR INFORMACIÓN </h1>
    </div>
    <form>
        <div class="modal-body">
            <div class="row form-group">
                <div class="col-3">
                    <label for="nombre"> Nombre: </label>
                </div>
                <div class="col-9">
                    <input class="form-control" id="nombre">
                </div>
            </div>
            <div class="row form-group">
                <div class="col-3">
                    <label for="correo"> Correo: </label>
                </div>
                <div class="col-9">
                    <input class="form-control" id="correo">
                </div>
            </div>
            <div class="row form-group">
                <div class="col-3">
                    <label for="edificio"> Edificio: </label>
                </div>
                <div class="col-9">
                    <input class="form-control" id="edificio">
                </div>
            </div>
            <div class="row form-group">
                <div class="col-3">
                    <label for="departamento"> Departamento: </label>
                </div>
                <div class="col-9">
                    <input class="form-control" id="departamento">
                </div>
            </div>
            <div class="row form-group">
                <div class="col-3">
                    <label for="extensión"> Extensión: </label>
                </div>
                <div class="col-9">
                    <input class="form-control" id="extensión">
                </div>
            </div>
            <div class="row form-group">
                <div class="col-3">
                    <label for="pass"> Contaseña: </label>
                </div>
                <div class="col-9">
                    <input class="form-control" id="pass">
                </div>
            </div>
        </div>
        <button type="submit" class="btn btn-primary" style="width: 200px; margin-left: 40%;"> Aceptar </button>
    </form>
@endsection

@section('top')
    <a class="scroll-to-top rounded" href="#page-top">
        <i class="fas fa-angle-up"></i>
    </a>
@endsection
