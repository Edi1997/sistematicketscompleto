@include('layouts.app')

@section('bread-ticket')
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="#"> Inicio </a>
        </li>
        <li class="breadcrumb-item active"> Generar Ticket </li>
    </ol>
@endsection

@section('bread-index')
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="#"> Inicio </a>
        </li>
        <li class="breadcrumb-item active"> Ver Tickets </li>
    </ol>
@endsection

@section('bread-seguimiento')
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="#"> Inicio </a>
        </li>
        <li class="breadcrumb-item active"> Mis Tickets </li>
    </ol>
@endsection

@section('bread-acceso')
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="#"> Inicio </a>
        </li>
        <li class="breadcrumb-item active"> Solicitudes de Acceso </li>
    </ol>
@endsection

@section('bread-sticket')
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="#"> Inicio </a>
        </li>
        <li class="breadcrumb-item active"> Solicitudes de Tickets </li>
    </ol>
@endsection

@section('bread-usuarios')
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="#"> Inicio </a>
        </li>
        <li class="breadcrumb-item active"> Todos los Usuarios </li>
    </ol>
@endsection

@section('bread-perfil')
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="#"> Inicio </a>
        </li>
        <li class="breadcrumb-item active"> Editar Perfil </li>
    </ol>
@endsection
@section('bread-categorias')
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="#"> Inicio </a>
        </li>
        <li class="breadcrumb-item active">Categorías</li>
    </ol>

@endsection
@section('bread-licencias')
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="{{url('/tickets/')}}"> Inicio </a>
        </li>
        <li  class="breadcrumb-item active">  Alta Licencias </li>
    </ol>
@endsection
@section('bread-licenciasasig')
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="{{url('/tickets/')}}"> Inicio </a>
        </li>
        <li  class="breadcrumb-item active">  Asignación de Licencias </li>
    </ol>
@endsection
@section('bread-licenciasconsulta')
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="{{url('/tickets/')}}" > Inicio </a>
        </li>
        <li  class="breadcrumb-item active"> Consulta de Licencias </li>
    </ol>
@endsection
@section('bread-equipos')
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="{{url('/tickets/')}}" > Inicio </a>
        </li>
        <li  class="breadcrumb-item active"> Alta de equipos </li>
    </ol>
@endsection
@section('bread-mantenimiento')
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="{{url('/tickets/')}}" > Inicio </a>
        </li>
        <li  class="breadcrumb-item active"> Mantenimientos </li>
    </ol>
@endsection
@section('bread-editarmantenimiento')
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="{{url('/tickets/')}}" > Inicio </a>
        </li>
        <li  class="breadcrumb-item active"> Editar solicitud de mantenimiento </li>
    </ol>
@endsection
@section('bread-correos')
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="{{url('/tickets/')}}" > Inicio </a>
        </li>
        <li  class="breadcrumb-item active"> Datos de peticiones(No validadas) </li>
    </ol>
@endsection

