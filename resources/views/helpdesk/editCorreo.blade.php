@extends('correo')
@include('layouts.navbar')
@include('layouts.sidebarHelp')   
@section('solicitudes-tabla')
<div class="card mb-3">
 <div class="card-header">
        <i class="fas fa-table"></i>
       Editar datos de peticion
    </div>
    <div class="card-body">
 <div class="table-responsive">
 <form action="{{ url('/correos/'.$datos->id)}}" method="post" enctype="multipart/form-data" >
               {{csrf_field()}}
                {{method_field('PATCH')}}
                    <div class="form-group">
                    {{-- numero de control --}}
               <div class="form-group">
          <div class="form-row">
            <div class="col-md-6">
              <div class="form-label-group">
                <input type="text" id="numerocontrol" class="form-control" placeholder="Numero de control" required="required" name="numerocontrol"value="{{$datos->numerocontrol}}">
                <label for="numerocontrol"> Número de control</label>
              </div>
            </div>
        {{-- Carrera --}}
             <div class="col-md-6">
              <div class="form-label-group">
                <input type="text" id="carrera" class="form-control" placeholder="Carrera" required="required" name="carrera" value="{{$datos->carrera}}">
                <label for="carrera"> Carrera</label>
              </div>
            </div>
          </div>
        </div> 
   {{-- Apellidos --}}
 <div class="form-group">
          <div class="form-row">
            <div class="col-md-6">
          
              <div class="form-label-group">
                 
                <input type="text" id="apellidoPaterno" class="form-control" placeholder="Apellido Paterno" required="required" name="apellidoPaterno"value="{{$datos->apellidoPaterno}}">
                <label for="apellidoPaterno"> Apellido Paterno </label>
              </div>
            </div>
            <div class="col-md-6">
             
              <div class="form-label-group">
             
                <input type="text" id="apellidoMaterno" class="form-control" placeholder="Apellido Materno" required="required" name="apellidoMaterno"value="{{$datos->apellidoMaterno}}">
               <label for="apellidoMaterno"> Apellido Materno</label>
              </div>
            </div>
          </div>
        </div>
       {{-- Nombres --}}
    <div class="form-group">
          <div class="form-label-group">
            <input type="text" id="nombre" class="form-control" placeholder="Nombre" required="required" name="nombre"value="{{$datos->nombre}}">
            <label for="nombre">Nombre(s)</label>
          </div>
        </div>
          {{-- Opciones--}}
              <div class="form-group">
                        <div class="form-label-group">
                          <div class="form-group">
                  <select class="form-control" id="opcion" name="opcion"  required >
                    <option value="Correo Nuevo" @if(($datos->opcion)=="Correo Nuevo")  selected @endif>Correo Nuevo</option>
                    <option value="Falla con el Correo" @if(($datos->opcion)=="Falla con el Correo") selected @endif>Falla con el Correo</option>
                    <option value="Restablecer Contraseña"@if(($datos->opcion)=="Restablecer Contraseña") selected @endif>Restablecer Contraseña</option>
                  </select>
                   
                </div>
          </div>
        </div>
     {{-- Descripcion--}}
<div class="form-group">
    {{-- <label for="descripcion">Descripción del Problema</label> --}}
          <div class="form-label-group">
            <textarea type="text" class="form-control" id="descripcion" placeholder="Descripción del problema" name="descripcion" required="" >{{$datos->descripcion}}</textarea>
         {{-- <label for="descripcion">Descripción del Problema</label> --}}
          </div>
        </div>{{-- Email--}}
        <div class="form-label-group">
          <input type="email" id="email" class="form-control" placeholder="Ej.: usuario@servidor.com" required="required" name="email" value="{{$datos->email}}">
          <label for="email"> Email de Contacto, Ej.: usuario@servidor.com</label>
        </div>

{{-- Credencial--}}
<div class="form-group">
 <label for="file">Subir foto de credencial:(Tipos de archivo permitidos: JPG,JPEG,PNG)</label>
 <input type="file" name="foto" id="foto" value=""> 
          <div class="form-label-group">
          <img src=" {{ asset('storage').'/'.$datos->foto}}" alt="" width="400"> 
              
          </div>
        </div>

                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary"> Aceptar </button>
                        
                    </div>
                   
                </form>
               
      </div>
    </div>
</div>
@endsection