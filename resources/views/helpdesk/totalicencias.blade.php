@extends('licenciasconsulta')
@include('layouts.navbar')
@include('layouts.sidebarHelp')   
@section('solicitudes-tabla')
<div class="card mb-3">
      <div class="card-header">
        <i class="fas fa-table"></i>
        Usuarios
    </div>
    <div class="card-body">
        <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                <thead>
                    <tr>

                        <th> Nombre del programa</th>
                        <th> Digitos de licencia</th>
                        <th> Version</th>
                        <th> Subversion</th>
                        <th> Equipo</th>
                        <th> Nombre del usuario</th>
                        <th> Departamento </th>
                    </tr>
                </thead>
             <tbody>
                    @foreach($n as $informacion)
                    <tr>
                        <td>{{$informacion->nombre_programa}} </td>
                        <td>{{$informacion->digitos}} </td>
                        <td>{{$informacion->version}} </td>
                         <td>{{$informacion->subversion}} </td>
                        <td>{{$informacion->nserie_equipo}} </td>
                         <td>{{$informacion->name}} </td>
                        <td>{{$informacion->departamento}} </td>
                        

                   </tr>
                   @endforeach

               </tbody> 

           </table>
       </div>
   
   </div>
   <div class="card-footer small text-muted">Updated yesterday at 11:59 PM</div>
</div>
{{-- <a class="btn btn-warning" href="{{url('/tickets/')}}" > Regresar</a> --}}
@endsection