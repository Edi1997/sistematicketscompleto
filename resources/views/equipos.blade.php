@include('layouts.scripts')
@include('layouts.footer')
@include('layouts.breadcrumb')
          
<!DOCTYPE html>
    <html lang="en">
        <head>
            @include('layouts.head')
           
        </head>
        <body id="page-top">
            {{-- NAVBAR --}}
            @yield('navbar')
            @yield('logout')
            {{-- CONTENT --}}
            <div id="wrapper">
                @yield('sidebar')
                <div id="content-wrapper">
                    <div class="container-fluid">
                         @yield('bread-equipos')
                        {{-- <button class="btn btn-info" data-toggle="modal" data-target="#modalregistro" style="width: 200px; margin-left: 40%;"> Nueva asignación </button><br>  --}}
                        @yield('solicitudes-tabla')
                        @yield('content')
                        @yield('modaleditar')
                        @yield('modalregistro')
                    </div>
                    {{-- @yield('footer') --}}
                </div>
            </div>
            @yield('top')
        </body>
    </html>
    @yield('scripts')