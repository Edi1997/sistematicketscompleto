@extends('mantenimiento')
@include('layouts.navbar')
@include('layouts.sidebarAgente')  
@section('solicitudes-tabla')

<div class="card mb-3">

    <div class="card-header">
        <i class="fas fa-table"></i>
        Mantenimientos
    </div>
    <div class="card-body">
        <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                <thead>
                    <tr>
                      {{-- <th> Nombre del programa </th>
                        <th> Licencia Digitos</th> --}}
                         <th> Descargar PDF</th>
                        <th> Folio</th>
                        <th> Fecha</th>
                        <th> Área Solicitante</th>
                        <th> Responsable del Área</th>
                        <th> Tipo de servicio</th>
                         <th>Descripción del servicio o falla</th> 
                        <th> Trabajo o servicio realizado</th>
                        <th> Material Utilizado</th>
                        <th>Solicitante</th>
                         <th> Atendio</th>
                           {{-- <th> Opciones</th> --}}
                    </tr>
                </thead>
                <tbody>
               @foreach($mantenimientos as $mantenimiento )
                   <tr>
                   <td> <a href="{{ url('/mantenimiento/pdf/'.$mantenimiento->id) }}"class="btn btn-success "><svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-arrow-down-circle-fill" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
  <path fill-rule="evenodd" d="M16 8A8 8 0 1 1 0 8a8 8 0 0 1 16 0zM8.5 5a.5.5 0 0 0-1 0v4.793L5.354 7.646a.5.5 0 1 0-.708.708l3 3a.5.5 0 0 0 .708 0l3-3a.5.5 0 0 0-.708-.708L8.5 9.793V5z"/>
</svg> </td>
                    <td>{{ $mantenimiento->id}}</td>
                    <td>{{ $mantenimiento->fecha}}</td>
                    <td>{{ $mantenimiento->area}}</td>
                    <td>{{ $mantenimiento->responsable}}</td>
                    <td>{{ $mantenimiento->tiposervicio}}</td>
                    <td>{{ $mantenimiento->descripcion}}</td>
                    <td>{{ $mantenimiento->serviciorealizado}}</td>
                    <td>{{ $mantenimiento->material}}</td>
                    <td>{{ $mantenimiento->solicitante}}</td>
                    <td>{{ $mantenimiento->realizador}}</td>
                    {{-- <td>
                    <form method="post" action="{{url('/mantenimiento/'.$mantenimiento->id)}}">
                               {{csrf_field()}}
                               {{method_field('DELETE')}}

                               <button   class="btn btn-danger" type="submit" onclick="return confirm('¿Borrar?');"><i class="fa fa-trash"></i></button>                 
                           </form>
                    <a class="btn btn-warning" href="{{url('/mantenimiento/'.$mantenimiento->id.'/edit')}}" class="secondary-content"><i class="fa fa-edit"></i></a></a>
                    </td> --}}
                {{-- <td><a href="{{ url('/licencia_equipo/show/'.$licencia->id)}}" class="btn btn-success ">Asignar licencias</a>  </td> --}}
                   </tr>
               @endforeach

               </tbody> 
           </table>
             {{-- <a  href="{{ route('exportar') }}"  class="btn btn-info" style="width: 200px">Exportar a CSV</a>  --}}
              {{-- href="/tick/public/csv" --}}
       </div>
   </div>
   <div class="card-footer small text-muted">Updated yesterday at 11:59 PM</div>
</div>
{{-- <a class="btn btn-warning" href="{{url('/tickets/')}}" > Regresar</a> --}}
@endsection

@section('modalregistro')
<div class="modal fade" id="modalregistro" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header text-center">
                <h5 class="modal-title text-center" id="exampleModalLabel"> Solicitud de mantenimiento </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>


            <form action="{{route('mantenimiento.store')}}" method="post" >
                {{csrf_field()}}
                <div class="modal-body">
                   
                 <div class="form-group">
                 <label for="fecha" name="fecha" id="fecha" > Fecha: </label>
                 <input  type="date" class="form-control" name="fecha" id="fecha" required >
                </div>  
                

                      <label > Área Solicitante : (Escribe el área solicitante o despliega las opciones (Departamentos))</label><br>
<input list="area" name="area" placeholder="Doble clic menú desplegable de opciones (Departamentos)" class="form-control" style="height: 50px; width: 460px;" required>
<datalist id="area">
  <option value="Ciencias Económico - Administrativo">Ciencias Económico - Administrativo</option>
                  <option value="DEPI">DEPI</option>
                  <option value="Eléctrica">Eléctrica</option>
                  <option value="Gestión Empresarial">Gestión Empresarial</option>
                  <option value="Ingeniería Bioquimica">Ingeniería Bioquimica</option>
                  <option value="Ingeniería Industrial">Ingeniería Industrial</option>
                  <option value="Ingeniería en Materiales">Ingeniería en Materiales</option>
                  <option value="Ingeniería Mécanica">Ingeniería Mécanica</option>
                  <option value="Ingeniería Mecatrónica">Ingeniería Mecatrónica</option>
                  <option value="Posgrado de Eléctrica">Posgrado de Eléctrica</option>
                  <option value="Sistemas y Computación">Sistemas y Computación</option>
</datalist>

   
<br>
 <label  > Responsable del Área: </label><br>
<input list="responsable" name="responsable" placeholder="Centro de cómputo o categoría" class="form-control" style="height: 50px;" required="">
<datalist id="responsable">
  <option value="Centro de Cómputo">Centro de Cómputo</option>
</datalist>
<br>
  
 
  

<label > Tipo de servicio </label><br>
<label style="margin-left: 80px;" > Recursos Materiales y Servicios: </label><br>
 <div class = "row">
               <div class = "input-field col s3">
                   <p>
                     <input id = "tiposervicio" type = "radio" name = "tiposervicio"
                        value = "Limpieza"  />
                     <label for = "tiposervicio">Limpieza</label>
                  </p>
                  
                  <p>
                     <input id = "tiposervicio" type = "radio" name = "tiposervicio"
                        value = "Cerrajería"  />
                     <label for = "tiposervicio">Cerrajería</label>
                  </p>
              
                <p>
                     <input id = "tiposervicio" type = "radio" name = "tiposervicio"
                        value = "Evento"  />
                     <label for = "tiposervicio">Evento</label>
                  </p>
                <p>
                     <input id = "tiposervicio" type = "radio" name = "tiposervicio"
                        value = "Mobiliario"  />
                     <label for = "tiposervicio">Mobiliario</label>
                  </p>
                  <p>
                     <input id = "tiposervicio" type = "radio" name = "tiposervicio"
                        value = "Otro"  />
                     <label for = "tiposervicio">Otro</label>
                  </p>
             </div>
       </div>
       <label style="margin-left: 80px;" > Centro de Cómputo</label><br>
 <div class = "row">
               <div class = "input-field col s3">
                   <p>
                     <input id = "tiposervicio" type = "radio" name = "tiposervicio"
                        value = "Hardware"  />
                     <label for = "tiposervicio">Hardware</label>
                  </p>
                  
                  <p>
                     <input id = "tiposervicio" type = "radio" name = "tiposervicio"
                        value = "Software"  />
                     <label for = "tiposervicio">Software</label>
                  </p>
              
                <p>
                     <input id = "tiposervicio" type = "radio" name = "tiposervicio"
                        value = "Telefónia"  />
                     <label for = "tiposervicio">Telefónia</label>
                  </p>
                <p>
                     <input id = "tiposervicio" type = "radio" name = "tiposervicio"
                        value = "Internet"  />
                     <label for = "tiposervicio">Internet</label>
                  </p>
                  <p>
                     <input id = "tiposervicio" type = "radio" name = "tiposervicio"
                        value = "Redes"  />
                     <label for = "tiposervicio">Redes</label>
                  </p>
                  <p>
                     <input id = "tiposervicio" type = "radio" name = "tiposervicio"
                        value = "Otro"  />
                     <label for = "tiposervicio">Otro</label>
                  </p>
             </div>
       </div>
                    <div class="row form-group">
                        <div style="margin-left: 30px;">
                            <label for="descripcion"> Descripción del servicio que solicita o falla a reparar: </label>
                        </div>
                        <div class="col-9">
                            {{-- <input type="text" class="form-control" id="version" name="version" required=""> --}}
                            <textarea type="text" class="form-control" id="descripcion" name="descripcion" required=""style="height: 68px; width: 366px; margin-left: 30px;"></textarea>
                        </div>
                    </div>

                    <div class="row form-group">
                        <div style="margin-left: 30px;">
                            <label for="serviciorealizado"> Trabajo o servicio realizado: </label>
                        </div>
                        <div class="col-9">
                            {{-- <input type="text" class="form-control" id="version" name="version" required=""> --}}
                            <textarea type="text" class="form-control" id="serviciorealizado" name="serviciorealizado" required=""style="height: 68px; width: 366px; margin-left: 30px;" ></textarea>
                        </div>
                    </div>

                    <div class="row form-group">
                        <div style="margin-left: 30px;">
                            <label for="material"> Material Utilizado: </label>
                        </div>
                        <div class="col-9">
                            {{-- <input type="text" class="form-control" id="version" name="version" required=""> --}}
                            <textarea type="text" class="form-control" id="material" name="material" required=""style="height: 68px; width: 366px; margin-left: 30px;" ></textarea>
                        </div>
                    </div>


                    <div class="row form-group">
                        <div class="col-3">
                            <label for="solicitante"> Solicita (Nombre): </label>
                        </div>
                        <div class="col-9">
                            <input class="form-control" id="solicitante" name="solicitante" required>
                        </div>
                    </div>

                    <div class="row form-group">
                        <div class="col-3">
                            <label for="realizador"> Atendió (Nombre): </label>
                        </div>
                        <div class="col-9">
                            <input class="form-control" id="realizador" name="realizador"  required>
                        </div>
                    </div>

                     <div class="row form-group">
                        <div class="col-3">
                            <label for="recibidor"> Vo.Bo (Nombre)</label>
                        </div>
                        <div class="col-9">
                            <input class="form-control" id="recibidor" name="recibidor" required>
                        </div>
                    </div>
             
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal"> Cancelar </button>
                <button type="submit" class="btn btn-primary"> Aceptar </button>
            </div>
        </form>
    </div>
    {{-- <a class="btn btn-warning" href="{{url('/tickets/')}}" > Regresar</a> --}}
</div>
</div>
@endsection






