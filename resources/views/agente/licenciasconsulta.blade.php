@extends('licenciasconsulta')
@include('layouts.navbar')
@include('layouts.sidebarAgente')  
@section('solicitudes-tabla')
<div class="card mb-3">
    <div class="card-header">
        <i class="fas fa-table"></i>
        Listado de licencias asignadas
    </div>
    <div class="card-body">
        <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                <thead>
                    <tr>
                      {{-- <th> Nombre del programa </th>
                        <th> Licencia Digitos</th> --}}
                        <th> Usuario</th>
                        <th> Departamento</th>
                        <th> Equipo (Numero de serie activo)</th>
                         <th>Detalles Licencias</th> 
                    </tr>
                </thead>
               <tbody>
               @foreach($equipos as $equipo )
                   <tr>
                     <td>{{ $equipo->user->name}}</td>
                    <td>{{ $equipo->user->departamento}}</td> 
                    <td>{{ $equipo->nserie_equipo}}</td> 
                    <td><a href="{{ url('/equipo/numero/'.$equipo->id)}}" class="btn btn-success ">Información Completa</a> </td> 
                   </tr>
               @endforeach

               </tbody> 
           </table>
            <a  href="{{ route('exportar') }}"  class="btn btn-info" style="width: 200px">Exportar a CSV</a> 
       </div>
   </div>
   <div class="card-footer small text-muted">Updated yesterday at 11:59 PM</div>
</div>
{{-- <a class="btn btn-warning" href="{{url('/tickets/')}}" > Regresar</a> --}}
@endsection
