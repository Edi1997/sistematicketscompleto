@extends('licenciasasignacion')
@include('layouts.navbar')
@include('layouts.sidebarAgente')  
 @section('solicitudes-tabla')
<div class="card mb-3">
    <div class="card-header">
        <i class="fas fa-table"></i>
       Agregar Equipos
    </div>
    <div class="card-body">
        <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                <thead>
                    <tr>
                        <th> Usuario</th>
                        <th> Departamento</th>
                        <th> Agregar Equipos</th>
                        {{-- <th> Número de Equipos-Detalles</th>  --}}
                    </tr>
                </thead>
               <tbody>
               @foreach($users as $user)
                   <tr>
                    <td>{{ $user->name}}</td>
                    <td>{{ $user->departamento}}</td>
                    <td><a href="{{ url('/equipo/show/'.$user->id)}}" class="btn btn-success ">Agregar Equipos</a>  </td>
                   {{-- <td><a href="{{ url('/equipo/numeroequipo/'.$user->id)}}" class="btn btn-success ">Detalles</a> </td>  --}}
                   </tr>
               @endforeach
               </tbody> 
           </table>
        
       </div>
   </div>
   <div class="card-footer small text-muted">Updated yesterday at 11:59 PM</div>
</div>
{{-- <a class="btn btn-warning" href="{{url('/tickets/')}}" > Regresar</a> --}}
@endsection 


