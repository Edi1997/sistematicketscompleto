@extends('licenciasasignacion')
@include('layouts.navbar')
@include('layouts.sidebarAgente')  
@section('solicitudes-tabla')
<div class="card mb-3">
    <div class="card-header">
        <i class="fas fa-table"></i>
        Listado de licencias
    </div>
    <div class="card-body">
        <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                <thead>
                    <tr>
                        <th> Usuario</th>
                         <th>Departamento</th>
                        <th> Equipo (Numero de serie activo)</th>
                        <th> Asignar licencias</th>
                        <th> Número de licencias</th>
                    </tr>
                </thead>
               <tbody>
               @foreach($licenciasUsuario as $licencia)
                   <tr>
                    <td>{{ $licencia->user->name}}</td>
                    <td>{{ $licencia->user->departamento}}</td>
                    <td>{{ $licencia->nserie_equipo}}</td>
               <td><a href="{{ url('/licencia_equipo/show/'.$licencia->id)}}" class="btn btn-success ">Asignar licencias</a>  </td>
                <td><a href="{{ url('/licencia_equipo/numero/'.$licencia->id)}}" class="btn btn-success ">Detalles</a> </td>
                   </tr>
               @endforeach
               </tbody> 
           </table>
       </div>
   </div>
   <div class="card-footer small text-muted">Updated yesterday at 11:59 PM</div>
</div>
 {{-- <a class="btn btn-warning" href="{{url('/tickets/')}}" > Regresar</a> --}}
@endsection
