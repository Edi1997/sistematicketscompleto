@extends('correo')
@include('layouts.navbar')
@include('layouts.sidebarAgente')  
@section('solicitudes-tabla')
<div class="card mb-3">
 <div class="card-header">
        <i class="fas fa-table"></i>
      Asignar estatus <br>
      Nombre: {{$datos->nombre}} {{$datos->apellidoPaterno}} {{$datos->apellidoMaterno}}<br>
      Numero Control: {{$datos->numerocontrol}}<br>
      Carrera: {{$datos->carrera}}
    </div>
    <div class="card-body">
 <div class="table-responsive">
  <form action="{{ url('/apoyo/'.$datos->id)}}" method="post" enctype="multipart/form-data" > 

               {{csrf_field()}}
                {{method_field('PATCH')}}
                    <div class="form-group">
                  
       
          {{-- Opciones--}}
              <div class="form-group">
                        <div class="form-label-group">
                          <div class="form-group">
                  <select class="form-control" id="estatus" name="estatus"  required >
                    <option value="Recibido" @if(($datos->estatus)=="Recibido")  selected @endif>Recibido</option>
                    <option value="Atendido" @if(($datos->estatus)=="Atendido")  selected @endif>Atendido</option>
                    <option value="Cancelado"@if(($datos->estatus)=="Cancelado")  selected @endif>Cancelado</option>
                  </select>
                   
                </div>
          </div>
        </div>
    

                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary"> Aceptar </button>
                        
                    </div>
                   
                </form>
               
      </div>
    </div>
</div>
@endsection