@extends('seguimiento')
@include('layouts.navbar')
@include('layouts.sidebarAgente')

@section('progreso')
    <div class="text-center">
        <h3> PROGRESO </h3><br>
    </div>
    <div class="progress" style="height: 50px;" >
        <div class="progress-bar progress-bar-striped" id="procesando" style="width: 25%; background-color: dimgray;">
            <span style="font-size: 18px;"> Procesando </span>
        </div>
        <div class="progress-bar progress-bar-striped" id="aceptado" style="width: 25%; background-color: dimgray;">
            <span style="font-size: 18px;"> Aceptada </span>
        </div>
        <div class="progress-bar progress-bar-striped" id="progreso" style="width: 25%; background-color: dimgray;">
            <span style="font-size: 18px;"> En progreso </span>
        </div>
        <div class="progress-bar progress-bar-striped" id="finalizado" style="width: 25%; background-color: dimgray;">
            <span style="font-size: 18px;"> Finalizada </span>
        </div>
    </div>
@endsection

@section('tickets')
    <div class="card mb-3">
        <div class="card-header">
            <i class="fas fa-table"></i>
            Tickets
        </div>
        <div class="card-body">
            <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                <thead>
                <tr>
                    <th> Prioridad </th>
                    <th> Estado </th>
                    <th> Equipo </th>
                    <th> Tipo </th>
                    <th> Fecha de recepción </th>
                    <th> Fecha de finalización </th>
                    <th> Progreso </th>
                </tr>
                </thead>
                <tfoot>
                <tr>
                    <th> Prioridad </th>
                    <th> Estado </th>
                    <th> Equipo </th>
                    <th> Tipo </th>
                    <th> Fecha de recepción </th>
                    <th> Fecha de finalización </th>
                    <th> Progreso </th>
                </tr>
                </tfoot>
                <tbody>
               
               <?php
                    
                    foreach ($tickets as $ticket) {
                        echo "<tr>";

                        switch ($ticket->nivel) {
                            case 'Urgente':
                            echo '<td class="bg-danger"> '.$ticket->nivel.' </td>';
                            break;
                            case 'Alta':
                            echo '<td style="background-color: orange;"> '.$ticket->nivel.' </td>';
                            break;
                            case 'Media':
                            echo '<td style="background-color: rgb(233, 230, 60);"> '.$ticket->nivel.' </td>';
                            break;
                            case 'Baja':
                            echo '<td class="bg-primary"> '.$ticket->nivel.' </td>';
                            break;
                            default:
                            break;
                        }
                        echo "<td>".$ticket->estatus."</td>";
                        echo "<td>".$ticket->equipoReportado."</td>";
                        echo "<td>".$ticket->categoriaReporte."</td>";
                        echo "<td>".$ticket->created_at."</td>";
                        echo "<td>".$ticket->updated_at."</td>";
                        echo "<input value='".$ticket->estatus."' name='estatus' id='estatus' hidden >";
                        echo "<td><button type='submit' onclick=enviarEstatus('".$ticket->estatus."') class='btn btn-primary btn-block'> Ver</button></td>";
                        
                        echo "</tr>";
                    }
                    ?>
                
              
                </tbody>
            </table>
            </div>
        </div>
        
    <div class="card-footer small text-muted">Updated yesterday at 11:59 PM</div>
    </div>
@endsection

@section('top')
    <a class="scroll-to-top rounded" href="#page-top">
        <i class="fas fa-angle-up"></i>
    </a>
@endsection

@section('scripts')
<script>
    function enviarEstatus(estatus){
        
        

        switch (estatus){
            case 'Abierto':
                    var divProcesa=document.getElementById('procesando');
                    divProcesa.style.backgroundColor = "red";
                    var divProcesa=document.getElementById('aceptado');
                    divProcesa.style.backgroundColor = "dimgray";
                    var divProcesa=document.getElementById('progreso');
                    divProcesa.style.backgroundColor = "dimgray";
                    var divProcesa=document.getElementById('finalizado');
                    divProcesa.style.backgroundColor = "dimgray";
                break;

            case 'En Espera ':
                    var divProcesa=document.getElementById('procesando');
                    divProcesa.style.backgroundColor = "red";
                    var divProcesa=document.getElementById('aceptado');
                    divProcesa.style.backgroundColor = "royalblue";
                    var divProcesa=document.getElementById('progreso');
                    divProcesa.style.backgroundColor = "dimgray";
                    var divProcesa=document.getElementById('finalizado');
                    divProcesa.style.backgroundColor = "dimgray";
                break;
            case 'En progreso':
                    var divProcesa=document.getElementById('procesando');
                    divProcesa.style.backgroundColor = "red";
                    var divProcesa=document.getElementById('aceptado');
                    divProcesa.style.backgroundColor = "royalblue";
                    var divProcesa=document.getElementById('progreso');
                    divProcesa.style.backgroundColor = "orange";
                    var divProcesa=document.getElementById('finalizado');
                    divProcesa.style.backgroundColor = "dimgray";
                break;

        case 'Terminado':
                    var divProcesa=document.getElementById('procesando');
                    divProcesa.style.backgroundColor = "red";
                    var divProcesa=document.getElementById('aceptado');
                    divProcesa.style.backgroundColor = "royalblue";
                    var divProcesa=document.getElementById('progreso');
                    divProcesa.style.backgroundColor = "orange";
                    var divProcesa=document.getElementById('finalizado');
                    divProcesa.style.backgroundColor = "green";
                break;

        case 'Cerrado':
                    var divProcesa=document.getElementById('procesando');
                    divProcesa.style.backgroundColor = "green";
                    var divProcesa=document.getElementById('aceptado');
                    divProcesa.style.backgroundColor = "green";
                    var divProcesa=document.getElementById('progreso');
                    divProcesa.style.backgroundColor = "green";
                    var divProcesa=document.getElementById('finalizado');
                    divProcesa.style.backgroundColor = "green";
                break;
            default:
                    var divProcesa=document.getElementById('procesando');
                    divProcesa.style.backgroundColor = "dimgray";
                    var divProcesa=document.getElementById('aceptado');
                    divProcesa.style.backgroundColor = "dimgray";
                    var divProcesa=document.getElementById('progreso');
                    divProcesa.style.backgroundColor = "dimgray";
                    var divProcesa=document.getElementById('finalizado');
                    divProcesa.style.backgroundColor = "dimgray";
        }
    }
</script>