@extends('licenciasasignacion')
@include('layouts.navbar')
@include('layouts.sidebarAgente')  
@section('solicitudes-tabla')
<div class="card mb-3">
<h5>Agregar Equipo del usuario: {{$user->name}}<br>
Departamento {{$user->departamento}}</h5>
{{-- <h5>Licencias Diponibles: </h5> --}}
<h5>Numero de serie del equipo (Activo)</h5>
<br>
<form style="margin-left:180px" class="col s12" method="post" action="{{ route('equipo.store',$user->id )}}">
 @csrf
             <div class="row form-group">
                        <div class="col-6">
                            <label for="nserie_equipo"> Equipo (Numero de serie activo): </label>
                        </div>
                        <div class="col-9">
                            <input class="form-control" id="nserie_equipo" name="nserie_equipo" placeholder="Escribe el numero de serie del equipo:">
                            {{-- <a href="{{ url('/licencia_usuario/equipolicencia/'.$licenciasusuario->id)}}" class="btn btn-success ">Asignar licencias al equipo</a>  --}}
                        </div>
             </div>  
             
         <br> <div class="footer">
                {{-- <button type="submit" class="btn btn-primary" > Aceptar </button> --}}
                <button class="btn btn-primary" type="submit" onclick="return confirm('¿Guardar? Verifique antes que la información del equipo este correcta')">Aceptar </button>
            </div>
            
        </div>
         {{-- <a class="btn btn-warning" href="{{url('/licencia_usuario/create')}}" > Regresar</a> --}}
        </div>
         
 </form>
 
 </div>
 
@endsection