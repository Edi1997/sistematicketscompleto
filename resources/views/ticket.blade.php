@include('layouts.scripts')
@include('layouts.footer')
@include('layouts.breadcrumb')

<!DOCTYPE html>
    <html lang="en">
        <head>
            @include('layouts.head')
        </head>
        <body id="page-top">
            {{-- NAVBAR --}}
            @yield('navbar')
            @yield('logout')
            {{-- CONTENT --}}
            <div id="wrapper">
                @yield('sidebar')
                <div id="content-wrapper">
                    <div class="container-fluid">
                        @yield('bread-ticket')
                        @yield('solicitud')
                    </div>
                    @yield('footer')
                </div>
            </div>
            @yield('top')
        </body>       
        <script>
            function otro() {
                // var opc= document.getElementById("selectEquipo").value;          
                        // //No hay campo de                     especificacion para otra opcion
                // var opc1= document.getElementById("selectTipoReporte").value;
                        // //    29/11 Isacc y Daniela
                var opc2= document.getElementById("selectUrgencia").value;
                // if (opc2==4){                    
                //     mostrarInput(1);
                // }else{
                //     ocultarInput(1);
                // }
                // if (opc1==4){
                //     mostrarInput(2);
                // }else{
                //     ocultarInput(2);
                // }
                if (opc2==1){
                    mostrarInput(3);
                }else{
                    ocultarInput(3);
                }
            }
            function ocultarInput(id){
                if (id==1){
                    document.getElementById('inputEquipo').style.display='none'; 
                    document.getElementById('inputEquipo').value=" ";
                }else if (id==2){
                    document.getElementById('inputtipoReporte').style.display='none'; 
                    document.getElementById('inputtipoReporte').value=" ";
                }else if(id==3){                 
                    document.getElementById('textUrgencia').style.display='none';                     
                    document.getElementById('labelUrgencia').style.display='none';                       
                    document.getElementById('textUrgencia').value=" ";   
                }       
            }
            function mostrarInput(id) {
                if(id==1){                    
                    document.getElementById('inputEquipo').style.display='block'; 
                }else if(id==2){
                    document.getElementById('inputtipoReporte').style.display='block'; 
                }else if(id==3){
                    document.getElementById('textUrgencia').style.display='block';                     
                    document.getElementById('labelUrgencia').style.display='block';                    
                }           
            }
            function ocultarTodo() {
                document.getElementById('inputEquipo').style.display='none'; 
                document.getElementById('inputtipoReporte').style.display='none';                 
                document.getElementById('textUrgencia').style.display='none';                     
                document.getElementById('labelUrgencia').style.display='none';                                        
                document.getElementById('textUrgencia').value=" ";                
            }
            window.onload=ocultarTodo;
        </script>
    </html>
    @yield('scripts')
