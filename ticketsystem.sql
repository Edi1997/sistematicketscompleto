-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 04-02-2020 a las 17:02:20
-- Versión del servidor: 10.4.6-MariaDB
-- Versión de PHP: 7.3.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `ticketsystem`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `categoriareporte`
--

CREATE TABLE `categoriareporte` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `categoriaReporte` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `descripcion` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `categoriareporte`
--

INSERT INTO `categoriareporte` (`id`, `categoriaReporte`, `descripcion`) VALUES
(1, 'Hardware', 'Un componente de hardware presentá una falla'),
(2, 'Software', 'El equipo presenta una falla de software'),
(3, 'Configuracion', ''),
(5, 'Soporte tecnico', 'soporte'),
(7, 'Impresora', 'error de imp'),
(8, 'Telefonia', ''),
(9, 'Redes', ''),
(10, 'Internet', '');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `departamento`
--

CREATE TABLE `departamento` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `departamento` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `edificio` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `equiporeportado`
--

CREATE TABLE `equiporeportado` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `equipoReportado` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `descripcion` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `estatus`
--

CREATE TABLE `estatus` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `estatus` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `descripcion` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `estatus`
--

INSERT INTO `estatus` (`id`, `estatus`, `descripcion`) VALUES
(1, 'Abierto', 'El ticket está a la espera de su aceptacion o declinacion'),
(2, 'Aceptado', 'El ticket fue aceptado'),
(3, 'Rechazado', 'El ticket fue declinado'),
(5, 'Asignado', 'El ticket tiene un usuario responsable asignado'),
(6, 'En progreso', 'El usuario responsable ya está trabajando en el ticket'),
(8, 'Finalizado', 'El ticket fue concluido con exito');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1),
(4, '2019_11_13_215233_create_rol_table', 1),
(5, '2019_11_13_215313_create_nivel_urgencia_table', 1),
(6, '2019_11_13_215340_create_estatus_table', 1),
(7, '2019_11_13_215356_create_tipo_reporte_table', 1),
(8, '2019_11_13_215430_create_categoria_reporte_table', 1),
(9, '2019_11_13_231131_add_columns_to_users', 1),
(10, '2019_11_13_231919_create_usuario_rol_table', 1),
(11, '2019_11_14_002011_create_ticket_table', 1),
(12, '2019_11_27_054153_create_equipo_reportado_table', 2),
(13, '2019_11_27_054333_create_departamento_table', 2),
(14, '2019_12_05_135218_add_sub_categoria_to_ticket_table', 2),
(16, '2020_02_03_013812_add_columns_to_ticket', 3);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `nivelurgencia`
--

CREATE TABLE `nivelurgencia` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `nivel` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `descripcion` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `nivelurgencia`
--

INSERT INTO `nivelurgencia` (`id`, `nivel`, `descripcion`) VALUES
(1, 'Urgente', 'Resolucion en menos de 2 horas'),
(2, 'Alta', ''),
(3, 'Media', ''),
(4, 'Baja', ''),
(7, 'Finalizado', '');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `rol`
--

CREATE TABLE `rol` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `rol` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `descripcion` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `rol`
--

INSERT INTO `rol` (`id`, `rol`, `descripcion`) VALUES
(1, 'Administrador', ''),
(2, 'HelpDesk', ''),
(3, 'Agente 1 nivel', ''),
(4, 'Agente 2 nivel', ''),
(5, 'Usuario', '');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ticket`
--

CREATE TABLE `ticket` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `titulo` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `equipoReportado` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `idTipoReporte` bigint(20) UNSIGNED DEFAULT NULL,
  `idCategoriaReporte` bigint(20) UNSIGNED DEFAULT NULL,
  `idNivelUrgencia` bigint(20) UNSIGNED DEFAULT NULL,
  `idEstatus` bigint(20) UNSIGNED DEFAULT NULL,
  `idUsuarioTicket` bigint(20) UNSIGNED DEFAULT NULL,
  `idUsuarioAsignado` bigint(20) UNSIGNED DEFAULT NULL,
  `justificacions` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NULL DEFAULT NULL,
  `subCategoria` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `descServicio` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `servicioRealizado` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `materialUtilizado` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `folioTicket` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tiporeporte`
--

CREATE TABLE `tiporeporte` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `tipoReporte` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `descripcion` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `tiporeporte`
--

INSERT INTO `tiporeporte` (`id`, `tipoReporte`, `descripcion`) VALUES
(1, 'Falla', ''),
(2, 'Solicitud', ''),
(3, 'Actividad', ''),
(4, 'Otro', '');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `apellidoP` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `apellidoM` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `edificio` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `extension` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `usuario` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `departamento` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `activo` tinyint(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`, `apellidoP`, `apellidoM`, `edificio`, `extension`, `usuario`, `departamento`, `activo`) VALUES
(1, 'Administrador', 'centrocomputo@gmail.com', NULL, '$2y$10$6bO7gpP/mu7OnapZtRS/JeyqoNjMDTXCdueuLe5f7HS4k9oV0Jua6', NULL, '2020-02-04 21:19:57', '2020-02-04 21:19:57', '-', '-', 'AG', '234', 'Administrador', 'Sistemas y Computación', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuariorol`
--

CREATE TABLE `usuariorol` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `idUsuario` bigint(20) UNSIGNED DEFAULT NULL,
  `idRol` bigint(20) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `usuariorol`
--

INSERT INTO `usuariorol` (`id`, `idUsuario`, `idRol`) VALUES
(1, 1, 1);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `categoriareporte`
--
ALTER TABLE `categoriareporte`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `departamento`
--
ALTER TABLE `departamento`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `equiporeportado`
--
ALTER TABLE `equiporeportado`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `estatus`
--
ALTER TABLE `estatus`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `nivelurgencia`
--
ALTER TABLE `nivelurgencia`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indices de la tabla `rol`
--
ALTER TABLE `rol`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `ticket`
--
ALTER TABLE `ticket`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ticket_idtiporeporte_foreign` (`idTipoReporte`),
  ADD KEY `ticket_idcategoriareporte_foreign` (`idCategoriaReporte`),
  ADD KEY `ticket_idnivelurgencia_foreign` (`idNivelUrgencia`),
  ADD KEY `ticket_idestatus_foreign` (`idEstatus`),
  ADD KEY `ticket_idusuarioticket_foreign` (`idUsuarioTicket`),
  ADD KEY `ticket_idusuarioasignado_foreign` (`idUsuarioAsignado`);

--
-- Indices de la tabla `tiporeporte`
--
ALTER TABLE `tiporeporte`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- Indices de la tabla `usuariorol`
--
ALTER TABLE `usuariorol`
  ADD PRIMARY KEY (`id`),
  ADD KEY `usuariorol_idusuario_foreign` (`idUsuario`),
  ADD KEY `usuariorol_idrol_foreign` (`idRol`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `categoriareporte`
--
ALTER TABLE `categoriareporte`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT de la tabla `departamento`
--
ALTER TABLE `departamento`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `equiporeportado`
--
ALTER TABLE `equiporeportado`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `estatus`
--
ALTER TABLE `estatus`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT de la tabla `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT de la tabla `nivelurgencia`
--
ALTER TABLE `nivelurgencia`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT de la tabla `rol`
--
ALTER TABLE `rol`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT de la tabla `ticket`
--
ALTER TABLE `ticket`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `tiporeporte`
--
ALTER TABLE `tiporeporte`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `usuariorol`
--
ALTER TABLE `usuariorol`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `ticket`
--
ALTER TABLE `ticket`
  ADD CONSTRAINT `ticket_idcategoriareporte_foreign` FOREIGN KEY (`idCategoriaReporte`) REFERENCES `categoriareporte` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `ticket_idestatus_foreign` FOREIGN KEY (`idEstatus`) REFERENCES `estatus` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `ticket_idnivelurgencia_foreign` FOREIGN KEY (`idNivelUrgencia`) REFERENCES `nivelurgencia` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `ticket_idtiporeporte_foreign` FOREIGN KEY (`idTipoReporte`) REFERENCES `tiporeporte` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `ticket_idusuarioasignado_foreign` FOREIGN KEY (`idUsuarioAsignado`) REFERENCES `usuariorol` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `ticket_idusuarioticket_foreign` FOREIGN KEY (`idUsuarioTicket`) REFERENCES `usuariorol` (`id`) ON DELETE CASCADE;

--
-- Filtros para la tabla `usuariorol`
--
ALTER TABLE `usuariorol`
  ADD CONSTRAINT `usuariorol_idrol_foreign` FOREIGN KEY (`idRol`) REFERENCES `rol` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `usuariorol_idusuario_foreign` FOREIGN KEY (`idUsuario`) REFERENCES `users` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
