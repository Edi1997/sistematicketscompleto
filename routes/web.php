<?php

Route::get('/', function () {
    // return view('admin.index');
    return view ('layouts.login');
});
Route::post('/filtrar', 'TicketController@filtrar');
Route::get('/index', 'userController@index');
// Route::get('/index', function () {
//     return view('admin.index');
// });
Route::get('tickets/rejected/{id}', 'TicketController@rejected');

// Route::get('/usuarios', function () {
//     return view('admin.usuarios');
// });

Route::get('/nuevo-ticket', function () {
    return view('usuario.ticket');
});

//Sin autenticación
Route::get('seguimiento', 'TicketController@seguimientoTicket');
Route::get('esperaAprob', 'userController@esperaAprob');

Route::get('/perfil', function () {
    return view('layouts.perfil');
});

Route::get('/login', function () {
    return view('layouts.login');
});

Route::get('/registro', function () {
    return redirect('register');
});
//Agregadas para solicitud de acceso 3_12  
	Route::get('/solicitudes-usuarios', 'userController@solicitudesUsuarios');
	Route::post('/filtrar-solicitud', 'userController@filtrarSolicitud');
	Route::post('user/update', 'userController@updateAcceso');	
//Agregadas para el crud de categorias 4_12
    Route::get('/categorias', 'categoriaController@index');
    Route::post('categoria/delete', 'categoriaController@delete');
    Route::post('categoria/update', 'categoriaController@update');
    Route::post('categoria/store', 'categoriaController@store');
    Route::get('/agregarCategoria', function () {
        return view('admin.agregaCategoria');
    });
    Route::post('categoria/findCategoria', 'categoriaController@show');
//Agregadas para generar reportes
    Route::get('reportes', 'TicketController@reportesTickets');
    Route::post('reporteCreate', 'TicketController@crearReporte');
    Route::get('reporteSolicitud/{id}', 'TicketController@crearReporteSolicitud');
// Agregadas para la solicitud
    Route::get('/solicitudes-tickets', 'TicketController@solicitudes2');
    Route::post('/solicitudes-tickets/aceptarTicket', 'TicketController@aceptarTicket');

// rutas para el reporte de solicitud
    Route::get('/reporte', 'TicketController@reporteFinal');
    Route::post('/reporte/update', 'TicketController@reporteUpdate');

//rutas para las licencias
Route::resource('altalicencias','LicenciaController');
Route::resource('licencia_equipo','LicenciaEquipoController');
Route::get('licencia_equipo/create','LicenciaEquipoController@create')->name('licencia_equipo.create');
Route::get('licencia_equipo/numero/{id}','LicenciaEquipoController@numero')->name('licencia_equipo.numero');
Route::get('equipo/numero/{id}','EquipoController@numero')->name('equipo.numero');
 Route::resource('equipo','EquipoController');
 Route::get('equipo/create','EquipoController@create')->name('equipo.create');
 Route::get('equipo/show/{id}','EquipoController@show')->name('equipo.show');
Route::post('equipo/store/{id}','EquipoController@store')->name('equipo.store');
Route::get('index', function () {
    return redirect('equipo');
 });
 Route::get('licencia_equipo/show/{id}','LicenciaEquipoController@show')->name('licencia_equipo.show');
 Route::post('licencia_equipo/store/{id}','LicenciaEquipoController@store')->name('licencia_equipo.store');
 Route::get('csv', function(){
    $n= DB::table('equipo_licencia')
    ->select('licencias.nombre_programa','licencias.digitos','licencias.version','licencias.subversion','equipos.nserie_equipo','users.name','users.departamento')
     ->join('licencias','equipo_licencia.licencia_id','=','licencias.id')
     ->join('equipos','equipo_licencia.equipo_id','=','equipos.id')
     ->join('users','equipos.user_id','=','users.id')
     ->get();
     $filename = "licencias.csv";
     $handle = fopen($filename, 'w+');
     fputcsv($handle, array('nombre del programa', 'digitos','version','subversion','numero de serie del equipo','usuario','departamento'));
     foreach($n as $row) {
         fputcsv($handle, array( $row->nombre_programa,$row->digitos,$row->version,$row->subversion,$row->nserie_equipo,$row->name,$row->departamento));
     }
     fclose($handle);
 
     $headers = array(
         'Content-Type' => 'text/csv',
     );
 
  return Response::download($filename, 'licencias.csv', $headers);

 })->name('exportar');
 
//rutas para solicitud de mantenimiento
Route::resource('mantenimiento','MantenimientoController');
Route::get('mantenimiento/pdf/{id}','MantenimientoController@create_pdf')->name('mantenimiento.pdf');

//rutas para apoyo de correos
Route::resource('apoyo','ApoyoController');
Route::get('/refresh_captcha', 'ApoyoController@refreshCaptcha')->name('refresh');
Route::resource('correos','CorreoController');
Route::get('correos/destroy/{id}', 'CorreoController@destroy')->name('correos.destroy');
Route::get('correos/show/{id}', 'CorreoController@show')->name('correos.show');
Route::get('csvc', function(){
    $n= DB::table('apoyos')
    ->select('*')
    ->from('apoyos')
     ->get();
    
   $filename = "ApoyoCorreosCompleto.csv";
   $handle = fopen($filename, 'w+');
     fputcsv($handle, array('numero de control', 'carrera','apellido paterno','apellido materno','nombre','peticion','descripcion del problema'));
     foreach($n as $row) {
         fputcsv($handle, array( $row->numerocontrol,$row->carrera,$row->apellidoPaterno,$row->apellidoMaterno,$row->nombre,$row->opcion,$row->descripcion));
     }
     fclose($handle);
 
     $headers = array(
         'Content-Type' => 'text/csv',
     );
 
  return Response::download($filename, 'ApoyoCorreosCompleto.csv', $headers);

 })->name('exportarcompleta');



Route::post('tickets/update', 'TicketController@update');
Route::resource('tickets', 'TicketController');
Route::resource('usuarios', 'userController'); 
Route::get('/usuarios', 'userController@index');
Route::get('/home', function(){
	return redirect('tickets');
});
Auth::routes();

