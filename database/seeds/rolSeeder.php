<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;


class rolSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('rol')->insert([
            'rol' => 'Administrador',
            'descripcion' => 'Administrador'
        ],
        [
            'rol' => 'Usuario',
            'descripcion' => 'Usuario'
        ]);
    }
}
