<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTipoReporteTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tipoReporte', function (Blueprint $table) {
            //Datos de la tabla tipoReporte
            $table->bigIncrements('id');
            $table->string('tipoReporte');
            $table->string('descripcion');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tipoReporte', function (Blueprint $table) {
            //
        });
    }
}
