<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnsToApoyos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('apoyos', function (Blueprint $table) {
            //
            $table->string('numerocontrol')->after('id');
            $table->string('carrera')->after('numerocontrol');
            $table->string('nombre')->after('carrera');
            $table->string('apellidoPaterno')->after('nombre');
            $table->string('apellidoMaterno')->after('apellidoPaterno');
            $table->string('opcion')->after('apellidoMaterno');
            $table->text('descripcion')->after('opcion');
            $table->string('email')->after('descripcion');
            $table->string('foto')->after('email');
            $table->string('captcha')->after('foto');
            $table->string('order_nr')->after('captcha');
            $table->string('estatus')->after('order_nr')->default('Recibido');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('apoyos', function (Blueprint $table) {
            //
        });
    }
}
