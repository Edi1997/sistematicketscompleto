<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateNivelUrgenciaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('nivelUrgencia', function (Blueprint $table) {
            //Datos de la tabla nivelUrgencia
            $table->bigIncrements('id');
            $table->string('nivel');
            $table->string('descripcion');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('nivelUrgencia', function (Blueprint $table) {
            //
        });
    }
}
