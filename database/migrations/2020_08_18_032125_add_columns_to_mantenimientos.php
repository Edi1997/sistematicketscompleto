<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnsToMantenimientos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('mantenimientos', function (Blueprint $table) {
            //
            $table->string('fecha')->after('id');
            $table->string('area')->after('fecha');
            $table->string('responsable')->after('area');
            $table->string('tiposervicio')->after('responsable');
            $table->text('descripcion')->after('tiposervicio');
            $table->text('serviciorealizado')->after('descripcion');
            $table->text('material')->after('serviciorealizado');
            $table->string('solicitante')->after('material');
            $table->string('realizador')->after('solicitante');
            $table->string('recibidor')->after('realizador');
           
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('mantenimientos', function (Blueprint $table) {
            //
        });
    }
}
